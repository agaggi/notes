# Securing Jenkins

## Access Control

Access control in Jenkins is split into 2 parts:

- Authentication done by the security realm. Determines user identity and group memberships
- Authorization is done by an authorization strategy. Controls whether the user has a permission

**A common mistake is allowing users with limited permissions to initiate build on the built-in
controller node**.

### Security Realm

Jenkins supports a few different Security Realms:

**Jenkins' own user database** \
This is the default option and uses jenkins' built-in user data store for authentication instead
of relying on an external system.

**LDAP** \
Delegate all authentication to a configured LDAP server (both users and groups). This option is
more common for larger installations in organizations that already have a configured external
identity provider such as LDAP. Also supports Active Directory installations.

**Unix user/group database** \
Delegates the authentication to the underlying Unix OS-level user database on the Jenkins controller.
This mode allows for the re-use of Unix groups for authorization.

For example, everyone in a "developer" group has administrative access. To support this feature, you
will need to configure PAM.

*Unix allows users and groups to have the same name. If this is the case, a '@' can be used to
signify a group (e.g. @dev)*.

### Authorization

**Project-based Matrix Authorization Strategy**

![Screenshot 1](./images/01.PNG)

### Permissions

The most basic permission a user can have is "Overall/Read" and the highest is "Overall/Administer".

Permissions can be granted globally, on an item (e.g. a folder or job), on a build, etc.

## Managing Security

The "Configure Global Security" section of the web UI allows a Jenkins administrator to enable,
configure, or disable key security features

### TCP Port

Jenkins can use a TCP port to communicate with inbound agents. **By default, the port is disabled**.

There are 2 options for ports:

- Random: a random port is chosen that does not collide with any other ports Jenkins is using. A
  massive downside to random port is it changes each time Jenkin is booted.
- Fixed: a port is selected by an administrator and remains consistant across reboots. This makes
  it much easier to manage firewall rules allowing TCP-based agents to connect to the controller.

Also, as of Jenkins 2.217, in-bound agents may be configured to use WebSocket transport to connect
to Jenkins. In this case, no extra TCP port and security configurations are needed.

## Controller Isolation

What happens during a build is often more controlled by people rather than a Jenkins administrator.
This is due to:

- Jenkins users with Job/Configuration permissions
- Build script authors (`pom.xml`, `makefile`, etc.)
- Code authors (e.g. test code executed during a build)

These do not even consider other attack vectors such as NPM and Maven packages that can insert
malicious code.

**For these reasons, builds should be executed on other nodes rather than the built-in one**.

### Not Building on the Built-In Node

Agents, which are statically configured or provided by clouds, should be used instead of the built-in
node.

To configure build settings, go to "Manage Jenkins > Manage Nodes and Clouds". Set the number of
executors to 0 on the built-in node and this will disable building jobs on it.

## Securing Builds

### Isolating Distributed Builds

While simple Jenkins isntances building a single project may be able to operate with a single
static agent, more complex instances may require agents to be isolated from each other.

### Pull Request Abuse

Jenkins automatically builds new pull requests by default. This can be abused in the following ways:

- Spamming pull requests to trigger many new builds
- Malicious pull requests that modify build scripts (e.g. mining cryptocurrency)

Strategies to deal with these issues:

- Use cloud providers to offload computation and limit the number of agents that can be launched in
  parallel.
- Define `timeouts` in the Pipeline definition (i.e. Jenkinsfile) as it cannot be modified by users
  without permissions.
- Do not automatically build new pull requests and instead require committers to opt into into them.
  See the GitHub Label Filter plugin.

## Cross Site Request Forgery (CSRF) Protection

CSRF is a type of security vulnerability in web applications. Without protection, a Jenkins user or
administrator visiting another website could allow the owner of that website to execute commands
in Jenkins.

CSRF protection uses a token (called a crumb in Jenkins) created by Jenkins and sent to the user.
Any form submissions or similar action resulting in modifications (e.g. triggering builds) requires
that crumb to be provided.

The crumb contains information about the user it was created for, preventing anyone else from
performing actions.

### Configuring CSRF Protection

Go to "Manage Jenkins > Configure Global Security > CSRF Protection" and choose "Default Crumb Issuer".
This option encodes the following information in the hash of the crumb:

- User's username
- Web session ID
- User's IP address
- A salt unique to this Jenkins instance

## Rendering User Content

Many features of Jenkins and plugins serve files that can be viewed or downloaded from Jenkins. These
can be risks as Cross-Site Scripting attacks could be put into HTML files.

### Content Security Policy

By default, Jenkins uses a strict content security policy on files that could have come from
untrusted sources. Only CSS and images can be served from other files in Jenkins and this disables
Javascript along with other active elements.

The downside to this is it prevents a lot of useful functionality from working such as generating
detailed, dynamic HTML reports created during builds.

### Resource Root URL

An alternative to Jenkins' content security policy is allowing Jenkins to serve potentially untrusted
files from a different domain. You can go to "Manage Jenkins > Configure System" and under the "Serve
resource files from another domain" to configure domains.

If the resource root URL is defined, Jenkins will redirect requests for user-created resource files
from that domain. For this option to work properly:

- The resource root URL must be valid
- The Jenkins URL must be set and different from the resource root URL
- Once set, Jenkins will only serve resource URL requests via the resource root URL. All other requests
  will get *404 not found* responses.

**These URLs are accessible to anyone without authentication until they expire, so sharing the URL
is like sharing the file directly.**

- Resource root URLs expire in 30 minutes by default

## Handling Environment Variables

Any user who can add environment variables with a name they choose may be able to modify the behavior
of builds (i.e. making a variable called `PATH`).

## Markup Formatters

Jenkins allows users with appropriate permissions to enter descriptions of various objects such as
views, jobs, builds, etc. These descriptions are filtered by markup formatters. They serve 2 purposes:

1. Allow users to use rich text formatting for these descriptions
2. Protect other users from Cross-Site Scripting attacks

The formatter can be configured in "Manage Jenkins > Configure Global Security > Markup Formatter".

The default formatter "Plain text" renders all descriptions as entered. Unsafe characters such as
'<' and '&' are escaped and line-breaks are rendered as `<br />` tags.

### Security Considerations

Every user with an account and Overall/Read permissions can edit their profile that includes a
description box that is rendered using the configured markup formatter.

Therefore, it can be unsafe to configure a markup formatter that allows arbitrary HTML. Anyone with
an account can edit their profile and anyone accessing their profile may become a victim of a
Cross-Site Scripting attack.

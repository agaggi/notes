# Installing Jenkins

## Setup

Some things to remember:

- If you want to build for all branches, leave it blank
- Make sure the `jenkins` user on your server has a `known_hosts` file with the Git service in it
- You can get the secret for a webhook via <user>/configure/API Token
- Plugins: Git, Github, ...

## Kubernetes

### Creating a Jenkins Deployment File

The following is a framework for a `.yaml` configuration file for a Jenkins deployment:

```yaml
# jenkins-deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: jenkins
spec:
  replicas: 1
  selector:
    matchLabels:
      app: jenkins
  template:
    metadata:
      labels:
        app: jenkins
    spec:
      containers:
      - name: jenkins
        image: jenkins/jenkins:lts-jdk11
        ports:
        - containerPort: 8080
        volumeMounts:
        - name: jenkins-home
          mountPath: /var/jenkins_home
      volumes:
      - name: jenkins-home
        emptyDir: { }
```

If you make any changes to the file, write them and run the following command to create the
deployment execute:

```
kubectl create -f jenkins-deployment.yaml -n jenkins
```

To validate the deployment was successful:

```
kubectl get deployments -n jenkins
```

### Granting Access to the Jenkins Service

The Jenkins instance is deployed but still not accessible. The Jenkins pod has been assigned an IP
address that is internal to the Kubernetes cluster; the Jenkins pod needs to be exposed as a service.

In a local deployment, this involves creating a NodePort service type. A NodePort service exposes a
service on each node in the cluster. A simple service is shown below:

```yaml
# jenkins-service.yaml

apiVersion: v1
kind: Service
metadata:
  name: jenkins
spec:
  type: NodePort
  ports:
  - port: 8080
    targetPort: 8080
  selector:
    app: jenkins
```

The Service is type "NodePort". Other options are "ClusterIP" for internal cluster communication
and "LoadBalancer" for IP addresses assigned by a cloud provider (e.g. AWS Elastic IP).

To create and validate the service, run the following:

```
kubectl create -f jenkins-service.yaml -n jenkins
kubectl get services -n jenkins
```

### Accessing the Jenkins Dashboard

You need to look at the output of the command in the previous section to see what port has been mapped
to port 8080. Then, to get the IP address of the cluster:

```
minikube ip
```

The default user for Jenkins will be "admin". To get the initial password for Jenkins, run the following:

```
kubectl get pods -n jenkins
```

This will get the name of the pod Jenkins is running in. Then we can look at the logs and get the password.

```
kubectl logs <pod_name> -n Jenkins
```

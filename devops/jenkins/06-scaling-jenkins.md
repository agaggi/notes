# Scaling Jenkins

## Architecting for Scale

**Vertical growth** is when the load on a Jenkins controller is increased due to having more jobs
configured or orchestrating builds more frequently.

**Horizontal growth** is the creation of additional Jenkins controllers to help accomodate new teams
and projects rather than adding new teams or projects to an existing controller.

### Ask Yourself These Questions

**Do you have the resources to run a distributed build system?** \
If possible, it is recommended to set up dedicated build nodes that run separately from the Jenkins
controller.

**Do you have the resources to maintain multiple controllers?** \
Jenkins controllers require regular plugin updates, backups, upgrades, etc. Security settings and
roles have to be manually configured on each controller. Many other factors not mentioned.

**How mission critical are each team's projects?** \
Consider segregating the most vital projects to separate controllers to minimize the impact of a
single downed controller.

**How important is a fast startup time for your Jenkins instance?** \
The more jobs a controller has configured, the longer it will take to restart.

### Agent Communications

For a machine to be recognized as an agent, it needs to run a specific agent program to establish
bidirectional communication with the controller.

There are different ways to establish connection between an agent and a controller:

- **SSH connector**: the most preferred and stable way to establish connection. The public key of
  the controller must be in the `authorized_keys` file on the agent. Once the host and SSH key is
  defined for the new agent, Jenkins handles the rest.

There are others but the most likely used one is via SSH. Reference
https://www.jenkins.io/doc/book/scaling/architecting-for-scale/#agent-communications for more options.

#### Setting up Cloud Agents

There are plugins that can help assist with creating cloud-based agents:

- The EC2 Plugin lets Jenkins use AWS EC2 instances. These instances are dynamically created and
  discarded when not in use.
- The Azure VM Agents Plugin dynamically creates agents as Azure VMs based on provided configuration.
  Includes support for virtual network integration and subnet placement. Idle agents can be configured
  for automatic shutdown to save costs
- The JCloud plugin

### Calculating Needed Resources

$$\text{# of Jobs} = \text{# of Developers} * 3.333$$
$$\text{# of Controllers} = \frac{\text{# of Jobs}}{500}$$
$$\text{# of Executors} = \text{# of Jobs} * 0.03$$

Additionally, the amount of space the controller will take up must be anticipated. For instance, many
plugins may be installed in the future so you would want a little bit of redundancy.

### Anatomy of a $JENKINS_HOME

```
JENKINS_HOME
 +- config.xml                (Jenkins root configuration file)
 +- *.xml                     (other site-wide configuration files)
 +- identity.key              (RSA key pair that identifies an instance)
 +- secret.key                (deprecated key used for some plugins' secure operations)
 +- secret.key.not-so-secret  (used for validating _$JENKINS_HOME_ creation date)
 +- userContent               (files served under your https://server/userContent/)
 +- secrets                   (root directory for the secret+key for credential decryption)
     +- hudson.util.Secret        (used for encrypting some Jenkins data)
     +- master.key                (used for encrypting the hudson.util.Secret key)
     +- InstanceIdentity.KEY      (used to identity this instance)
 +- fingerprints              (stores fingerprint records, if any)
 +- plugins                   (root directory for all Jenkins plugins)
     +- [PLUGINNAME]              (sub directory for each plugin)
         +- META-INF                  (subdirectory for plugin manifest + pom.xml)
         +- WEB-INF                   (subdirectory for plugin jar(s) and licenses.xml)
     +- [PLUGINNAME].jpi          (.jpi or .hpi file for the plugin)
 +- jobs                      (root directory for all Jenkins jobs)
     +- [JOBNAME]                 (sub directory for each job)
         +- config.xml                (job configuration file)
         +- workspace                 (working directory for the version control system)
         +- latest                    (symbolic link to the last successful build)
         +- builds                    (stores past build records)
             +- [BUILD_ID]                (subdirectory for each build)
                 +- build.xml                 (build result summary)
                 +- log                       (log file)
                 +- changelog.xml             (change log)
     +- [FOLDERNAME]              (sub directory for each folder)
         +- config.xml                (folder configuration file)
         +- jobs                      (sub directory for all nested jobs)
```

### Scaling Jenkins with Kubernetes

You don't need agents to be pre-existing; you can create them on-the-fly. Using Kubernetes comes with
several advantages:

1. Autohealing is possible. If an agent becomes corrupt, Jenkins can remove it and create a new one.
2. Running builds in parallel. You no longer have to plan the executors and limit them. Jenkins will
   spin up an instance and run your build on it.
3. Even load distribution. Kubernetes will manage loads well and spin up the best available server.

#### Setting up with Kubernetes

The best way is to create your Jenkins controller through Docker using a Dockerfile so your configuration
can be easily replicated.

```Dockerfile
FROM jenkins/jenkins:lts-jdk11

# Support pipelines with the following plugins
RUN jenkins-plugin-cli --plugins kubernetes
```

Before building the image, if you use `minikube`, you need to keep in mind `minikube` runs on your local
machine *inside* a virtual machine. Therefore, `minikube` will not be able to see docker images unless you
build them directly inside the `minikube` environment. This can be done as follows:

```bash
eval $(minikube docker-env)
```

Going back to general situations, you can build your Docker image as follows:

```
docker build -t my-jenkins-image:1.1 .
```

This will result in the following:

```
$ docker images
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
my-jenkins-image              1.1                 d6e46aa2470d        10 minutes ago      969MB
```

At this point, make sure you [set up Jenkins via Kubernetes](00-installing-jenkins.md).

### Jenkins Agent Configuration

To configure the Jenkins agents, we need to know the URL of the Kubernetes cluster and the internal
URL of the Jenkins pod. You can get the Kubernetes controller URL by running:

```
kubectl cluster-info
```

The Jenkins pod URL is standard (port 8080) and you can get IP addresses following the steps below:

```
# Yields pod_id
kubectl get pods -n jenkins | grep jenkins

kubectl describe pod -n jenkins <pod_id>
```

### Kubernetes Plugin Configuration

Start by going to "Manage Jenkins > Manage Nodes and Clouds > Configure Clouds > Add a new cloud >
Kubernetes" and fill in the Kubernetes URL and Jenkins URL respectively.

Then, in the "Kubernetes Pod Template" section, we need to specify the image that will be used to
create agents. If you have unique requirements, create your own Dockerfile. Otherwise, you can use
the default agent image hosted on dockerhub

# Managing Jenkins

## Configuration as Code

Jenkins can be configured as code via a Yaml file. If you install the "Configuration as Code"
plugin, you will see an additional option in the "Manage Jenkins" dashboard. You can see the
current configuration by then clicking "View Configuration".

The Jenkins Configuration as Code (JCasC) file has 4 sections:

1. `jenkins` section defines the root Jenkins project (Manage Jenkins > Configure System &
   Manage Jenkins > Configure Nodes and Clouds)
2. `tool` section defines build tools that can be set via "Manage Jenkins > Global Tool Configuration"
3. `unclassified` section defines all other configurations including for plugins
4. `credentials` section defines credentials that can be set via "Manage Jenkins > Manage Credentials".
   *It is suggested to avoid putting this section in*

To edit the configuration file, you will need to go to `$JENKINS_HOME/jenkins.yaml`. Remember you need
the "Configuration as Code" plugin installed.

It is not *required* but *suggested* you restart jenkins after making configuration changes.

**It is best practice to implement all of your settings via UI or all via JCasC**.

### Plugin Configuration

1. Use the UI in Jenkins to install and configure the plugin. "Apply > Save" when done.
2. Your plugin settings will be under "Manage Jenkins > Configuration as Code > View Configuration".
3. Download the configuration file, making adjustments if needed. Reload Jenkins of changes are made.
4. Check if everthing is okay and tested. If so push to your SCM.

Additional information: https://www.jenkins.io/blog/2021/05/20/configure-plugins-with-jcasc/

## Managing plugins

To see available plugins, go to https://www.plugins.jenkins.io.

There are 2 ways to install plugins:

1. Using the "Plugin Manager" in the web UI
2. Using `install-plugin` via the Jenkins CLI

### Managing via the CLI

To install plugins via the CLI, you need to install the Jenkins CLI client and generate an API token.

To get the Jenkins CLI, go to "Dashboard > Jenkins CLI". There you can download a `.jar` file.

To generate the API token, go to your dashboard and go down to the "API Token" section. Save the generated
string to a file on your machine you are trying to connect to Jenkins.

Example - a credential file called `creds`:

```
{your username}:{generated string}
```

Then we can access Jenkins:

```
java -jar jenkins.jar -s {URL to Jenkins instance} -auth @{path to credentials} {command}

# Example
java -jar jenkins.jar -s http://localhost:8080/ -auth @creds install-plugin pipeline-utility-steps
```

Other plugin commands include:

- `enable-plugin`
- `disable-plugin`

**You will need to restart Jenkins to be able to use the plugin**.

```
java -jar jenkins.jar -s http://localhost:8080/ -auth @creds -restart
```

## About Jenkins

To get information about your install of Jenkins, go to "Manage Jenkins > About Jenkins". You
can get the following information:

- List of all 3rd-party libraries used for your release of Jenkins
- List of static resources that are installed
- List of installed plugins with their details included

Under "Manage Jenkins > System Configuration" you can see:

- System properties that can be used as arguments via the command line
- Environment variables of the system with their current values
- List of plugins installed
- Memory usage in the form of a graph

## Jenkins Features Controlled with System Properties

Jenkins has some "hidden" features that can be enabled through system properties.

### Usage

System properties are defined by passing `-Dproperty=value` to the Java CLI tool. Make sure to
pass these options before the `-jar` flag or they will be ignored.

**These features are experimental and can change over time. Do not rely on them.** \
See https://jenkins.io/doc/book/managing/system-properties

## Jenkins CLI

Jenkins can be accessed via SSH, the Jenkins CLI client, or via the `.jar` file distributed
with Jenkins.

For troubleshooting tips, see: https://www.jenkins.io/doc/book/managing/cli/

### Using CLI over SSH

**SSH is disabled by default**.

You can either choose the SSH port Jenkins uses or it will randomly choose one for you. \
To find the SSH port Jenkins is using:

```
curl -Lv http://{JENKINS_URL}/login 2>&1 | grep -i "x-ssh-endpoint"
```

#### Authentication

Jenkins must have "Overall/Read" permission to access the CLI. Users may require additional
permissions depending on the commands they enter (e.g. `sudo`).

SSH relies on public/private keys for authentication. You will need to upload your public key
into Jenkins.

### Common Commands

To see the full list of commands:

```
ssh -l {user} -p {port} localhost help
```

#### Build

The `build` command allows users, who have permission, to trigger builds from the command line.
The most basic situation is a build triggers and exit, but you can also pass other parameters,
poll SCM, etc.

#### Console

The `console` command retrieves output for a specified build or Pipeline run. When no build number is
specified, the `console` command will output the last completed build's output.

Literally `cat build.log`.

```
% ssh -l kohsuke -p 53801 localhost help console

java -jar jenkins-cli.jar console JOB [BUILD] [-f] [-n N]
Produces the console output of a specific build to stdout, as if you are doing 'cat build.log'
 JOB   : Name of the job
 BUILD : Build number or permalink to point to the build. Defaults to the last
         build
 -f    : If the build is in progress, stay around and append console output as
         it comes, like 'tail -f'
 -n N  : Display the last N lines
```

#### who-am-i

Just like the Linux command. Tells you who you are logged in as.

### Using the CLI Client

There may be some situations where the CLI client is better than using SSH. For example, HTTP is the
default protocol used so you do not need to open any more ports like you might have to do with SSH.

Using the CLI client requires downloading a custom `.jar` file whereas SSH does not. \
The CLI tool, however, may make it easier to perform tasks that are common in Jenkins environments.

## Script Console

**Groovy is a powerful language that can basically do anything Java does. This includes:**

- **Creating sub-processes and executing arbitrary commands on Jenkins controller and agents**
- **Reading files the Jenkins controller has access to on the host (e.g. /etc/passwd)**
- **Decrypt credentials configured within Jenkins**

**Scripts can affect the entire Jenkins infrastructure. Granting script access is essentially
administrator access.**

Talk on scripting security: https://www.youtube.com/watch?v=qaUPESDcsGg \
Learn Groovy: http://groovy-lang.org/learn.html

To access the script console, go to "Manage Jenkins > Script Console". \
To run the script console on agents, go to "Manage Jenkins > Manage Nodes".

## Managing Nodes

**Jenkins Controller** \
The Jenkins controller is the jenkins service itself and where it is installed. It acts as the brain
for deciding how and when tasks are run. Management tasks such as configuration, authorization, and
authentication are done via the controller through the webserver UI. Files written when a pipeline
executes are saved on the controller unless they are offloaded to an artifact repository.

**Nodes** \
Nodes are the machines where build agents run on. Jenkins will monitor disk space, temp space, free
swap, clock time, and response time. If any of these go outside of their threashold, the node will
be taken down.

There are 2 types of nodes: *agents* and the *built-in node* on the controller. **Using the built-in
node is discouraged for security, performance, and scalability reasons**. To disable running tasks on
the controller, set the number of *executors* to 0.

**Agents** \
Agents manage task execution on behalf of the Jenkins controller. An agent is actually a small Java
client that connects to the Jenkins Controller. Tools required for builds and tests can be installed
directly or in a container (Docker or Kubernetes).

**Executors** \
An executor is a slot for an execution task; it is essentially a thread. The number of executors defined
are essentially the number of concurrent tasks that can be run at once on the Node.

You must be conscious of available resources on your Node (i.e. CPU cores, RAM, and I/O bandwidth).

### Creating Agents

To create a new agent, you can click on "Build Executor Status".

When creating a new node, it is conventional, but not required, to make its name the hostname of the system.
It is also a good idea to give the node a description.

#### Setting Executors

When setting the number of executors, keep the following in mind:

- Keep the number of executors at 1 unless you are sure you need more
- **Never** have the number of executors exceed the number of CPU cores

#### Assigning Labels

For labels, make them what is or will be installed on the system. For example, if you will install docker,
add "docker" as a label for the Node.

It may also be a good idea to add the name of your agent as a label in case you need to test something on
a specific agent.

#### Usage

There will be 2 options:

- Use this agent as much as possible
- Only build jobs with label expressions matching this node

The first option will allow the node to be picked up when `agent any` is used in a Jenkinsfile
whereas the second does not.

#### Launch Method

There will be 3 options:

- Launch agent by connecting it to the master
- Launch agent via execution of command on the controller
- Launch agents via SSH

The first option is common on Windows based agents that do not have SSH. \
The third option is most common on Linux agents.

Under "advanced", there is an option to change the default SSH port in case you want to.

#### Availability

You have the option to schedule when an agent goes online/offline.

## In-Process Script Approval

To protect jenkins from malicious scripts, plugins execute user-provided scripts in a *Groovy Sandbox*
that isolates them from accessing internal APIs. This protection is provided by the *Script Security
plugin*.

There are 2 main components of in-process scripting: the Groovy Sandbox and Script Approval.

### Groovy Sandbox

Most scripts run in the Groovy Sandbox by default including all Jenkins Pipelines. The sandbox only
allows Groovy methods deemed "safe" to execute without prior approval. Scripts using the Groovy
Sandbox are **all** subject to the same restrictions, regardless of whether they were run by an
administrator or a normal user.

You can remove the Groovy Sandbox all together but will require administrators to review scripts
manually.

### Script Approval

Used by administrators when the Groovy Sandbox is disabled to create a whitelist of approved scripts
and methods.

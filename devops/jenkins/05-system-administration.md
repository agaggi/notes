# System Administration

## Backing Up / Restoring Jenkins

### Creating a Backup

Various methods can be used to create backups:

- Filesystem snapshots
- Plugins
- Shell scripting

#### Filesystem Snapshots

Filesystem snapshots provide maximum consistancy for backups. They also run faster than live backups,
reducing the possibility of copying the same data multiple times. Filesystem snapshots are supported
by:

- The Linux Logical Volume Manager (LVM)
- Linux btrfs
- Solaris ZFS (which also supports incremental backups)
- FreeBSD ZFS
- OpenZFS on Linux
- Some other file system architectures
- Many cloud providers
- Some separate storage devices also let you create snapshots at the storage level.

#### Plugins

You can search for the term 'backup'. Currently, only the "thinBackup" plugin is being maintained.

#### Shell Scripting

You can write your own shell script that copies files and directories to a backup location. You will
need to make it a cron job.

**Make sure you use time-identifiers when writing backups or you may overwrite another backup**.

#### Controller Keys

The controller key is used to encrypt data in the `secrets` directory that secures credentials. The
secrets file is stored at `$JENKINS_HOME/secrets/hudson.util.Secret` and its parent directory is
encrypted by `master.key`. If you want to restore a system backup, you will need this file.

**Treat this file like a private SSH key and give it to no one and do not include it in the backup**.

#### What Files Should be Backed Up?

`$JENKINS_HOME` \
Backing up this directory will preserve the entire Jenkins instance; however, `$JENKINS_HOME` includes
files that do not need to be backed up. Selecting specific folders to backup may be better in the
long run.

**Configuration files** \
These files are stored directly in `$JENKINS_HOME`. `config.xml` is the main Jenkins configuration
file. There are other `.xml` configuration files so it is good to backup `$JENKINS_HOME/*.xml`.

Configuration files can also be stored in an SCM repository.

**./jobs subdirectory** \
The `$JENKINS_HOME/jobs` directory contains information about all the jobs created in Jenkins.

- `./builds` — Contains build records
- `./builds/archive` — Contains archived artifacts
    - Back this up if it is important to retain these artifacts long-term
    - These can be very large and may make your backups very large
- `./workspace` — Contains files checked out from the SCM
    - It is usually not necessary to back up these files. You can perform a clean checkout after
      restoring the system.
- `./plugins/*.hpi` — Plugin packages with specific versions used on your system
- `./plugins/*.jpi` — Plugin packages with specific versions used on your system

#### What Files Should Not be Backed Up?

`war`, `cache`, and `tools` directories.

#### Validating a Backup

To test if your backup works, you can set `$JENKINS_HOME` a temporary location and restore your backup
to that directory.

```
export JENKINS_HOME=/mnt/backup-test
```

Then, specify a random port that does not collide with your original instance:

```
java -jar jenkins.war ---httpPort=9999
```

## Viewing Logs

When running `jenkins.war` manually with `java -jar jenkins.war`, all logging information is outputted
to standard out.

**Linux**: logs can be viewed by running `journalctl -u jenkins.service`. \
**Windows**: logs should be at `%JENKINS_HOME%/jenkins.out` and `%JENKINS_HOME%/jenkins.err`. \
**Mac OS**: logs should be at `/var/log/jenkins/jenkins.log`. \
**Docker**: logs can be viewed by running `docker logs {containerID}`.

## Authenticating Scripted Clients

To make scripted clients such as `wget` invoke operations that require authentication, use HTTP
authentication to specify the username and API token. The API token is in your personal configuration
page under "Configure".

### Using curl and wget

```
curl -X POST -L --user your-user-name:apiToken \
    https://jenkins.example.com/job/your_job/build
```

```
wget --auth-no-challenge \
    --user=user --password=apiToken \
    http://jenkins.example.com/job/your_job/build
```

## Managing systemd Services

See https://www.jenkins.io/doc/book/system-administration/systemd-services/index.html

# Pipeline

## Using a Jenkinsfile

A *continuous delivery* pipeline automates the process of getting software from version control
all the way to the hands of customers.

Jenkins can model pipelines as code through the use of a `Jenkinsfile`. This file is included in
the version-controlled repository.

There is *scripted* syntax and *declarative* syntax. Declarative syntax provides more options
to customize your pipeline. You should stay away from scripted syntax as much as possible as
declarative syntax is the modern standard.

A `Jenkinsfile` is included in your source code that tells Jenkins how to build and test your
project.

Pipelining in Jenkins was not always there. People had to use multiple "Freestyle Projects" and
chain them together. Pipelining is the modern approach to this method, where we can just define
different stages in a `Jenkinsfile`. Pipelines also have better durability because if they are
taken offline for some reason, they can continue where they left off whereas Freestyle Projects
cannot.

Pipeline concepts:

- A **Pipeline** is a user-defined model of a CD pipeline
- A **node** is a machine that is part of the Jenkins environment and can execute a Pipeline
- A **stage** block defines a subset of tasks to be performed through the Pipeline. Typically there
  are 3 stages: Build, Test, and Deploy
- A **step** is a single task

Example of a basic declarative Jenkinsfile:

```Jenkinsfile
pipeline {
    agent any

    stages {
        stage('Hello') {
            steps {
                sh 'echo "Hello World"'
                sh '''
                    ls -al
                    pwd
                '''
            }
        }
    }
}
```

### Timeouts, Retries, and more

`timeout` helps the controller not waste resources if a step cannot be completed and `retry`
does what it implies.

**Inputs should always have timeouts**

```Jenkinsfile
pipeline {
    agent any
    stages {
        stage('Deploy') {
            steps {
                retry(3) {
                    sh './flakey-deploy.sh'
                }

                timeout(time: 3, unit: 'MINUTES') {
                    sh './health-check.sh'
                }
            }
        }
    }
}
```

If we wanted to retry 5 times but not spend more than 3 minutes on the stage, we could do the
following:

```Jenkinsfile
timeout(time: 3, unit: 'MINUTES') {
    retry(5) {
        sh './flakey-deploy.sh'
    }
}
```

### Post-Pipeline

We can execute commands based on the status of our pipeline to do things such as cleanup etc.

```
pipeline {
    agent any
    stages {
        stage('Test') {
            steps {
                sh 'echo "Fail!"; exit 1'
            }
        }
    }
    post {
        always {
            echo 'This will always run'
        }
        success {
            echo 'This will run only if successful'
        }
        failure {
            echo 'This will run only if failed'
        }
        unstable {
            echo 'This will run only if the run was marked as unstable'
        }
        changed {
            echo 'This will run only if the state of the Pipeline has changed'
            echo 'For example, if the Pipeline was previously failing but is now successful'
        }
    }
}
```

You can also send notifications based on the status of the pipeline. See more at
https://www.jenkins.io/doc/pipeline/tour/post/

### Global Variable Reference

There are 3 types of variables that can be referenced in a Pipeline:

- `env` - exposes environmental variables such as `env.PATH` and `env.BUILD_ID`
- `param` - exposes all parameters defined for the Pipeline as a read-only Map
- `currentBuild` - can be used to reveal information about the Pipeline

**Variables are relative to your Jenkins configuration (i.e. plugins, etc.)**. \
To find these variables, go to ${YOUR_JENKINS_URL}/pipeline-syntax/globals.

### Configuring Execution Environments

The `agent` portion of the Jenkinsfile is required. It tells Jenkins how to execute the pipeline.

```Jenkinsfile
/* Requires the Docker Pipeline plugin */
pipeline {
    agent { docker { image 'python:3.10.7-alpine' } }
    stages {
        stage('build') {
            steps {
                sh 'python --version'
            }
        }
    }
}
```

We can also configure environmental variables:

```Jenkinsfile
...
...
environment {
    NAME = 'Alex'
}

stages {
    stage('Build') {
        steps {
            echo "Hello ${NAME}"
        }
    }
}
```

### Recording Tests and Artifacts

To save people the headache of looking at the pipeline output for failed test details, we can
output the results to a file. Typically XML resports are used.

Note: A pipeline that has failing tests will be marked in yellow and marked as "UNSTABLE".

```Jenkinsfile
pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                sh './gradlew build'
            }
        }

        stage('Test') {
            steps {
                sh './gradlew check'
            }
        }
    }
    post {
        always {
            archiveArtifacts artifacts: 'build/libs/**/*.jar', fingerprint: true
            junit 'build/reports/**/*.xml'
        }

        failure {
            mail to: team@example.com, subject: 'Pipeline failed'
    }
}
```

When there are test failures, it can be a good idea to look at the generated artifacts to see
what went wrong. Jenkins supports artifact storing by default.

### Deployment

A Jenkinsfile should have at least 3 components:

```Jenkinsfile
pipeline {
    agent any
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Build') {
            steps {
                echo 'Building'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying'
            }
        }
    }
}
```

If you really want to, you can have multiple stages of *Deploy* (e.g. Staging and Production).

Additionally, you may want to have human intervention in your pipeline. You could do something like
this:

```Jenkinsfile
stage('Sanity check') {
    steps {
        input "Does the staging environment look ok?"
    }
}
```

### Using Multiple Agents

We can configure multiple agents to be used in a single Jenkinsfile:

```
pipeline {
    agent none
    stages {
        stage('Build') {
            agent any
            steps {
                checkout scm
                sh 'make'
                stash includes: '**/target/*.jar', name: 'app'
            }
        }
        stage('Test on Linux') {
            agent {
                label 'linux'
            }
            steps {
                unstash 'app'
                sh 'make check'
            }
            post {
                always {
                    junit '**/target/*.xml'
                }
            }
        }
        stage('Test on Windows') {
            agent {
                label 'windows'
            }
            steps {
                unstash 'app'
                bat 'make check'
            }
            post {
                always {
                    junit '**/target/*.xml'
                }
            }
        }
    }
}
```

### Job Scheduling

To schedule a job, go to `<Job>/Pipeline Syntax/Declarative Directive Generator` and select the
"triggers: Triggers" option under Sample Directive. Then select "cron" to make a cronjob.

You can click the blue question mark for help.

To schedule a job for midnight:

```
H 0 * * *
```

Note that there is an *H* instead of 0 to balance the load over time. We wouldn't want a bunch of
jobs scheduled at midnight to be fighting over resources so spreading them out slightly helps.

There is an alias for midnight, `@midnight`, that schedules jobs randomly between 0000 < x < 0300.

By default, UTC time is used. If you wanted to schedule a job for your timezone (e.g. Eastern US),
you can do:

```
TZ=America/New_York
H 0 * * *
```

To run a job every hour:

```
H */1 * * *
```

After you save your configuration, a `trigger` scope will be generated. **You need to add this
section to your Jenkinsfile AND run your job at least once more**.

## Branches and Pull Requests

If you are working with a project that has multiple branches, you may want to use a **Multi-branch
Pipeline**.

Multi-branch jobs make it so you do not have to maintain a separate Jenkinsfile for each branch
in your Git repository.

**A multi-branch job is effectively a folder of Pipeline jobs.**

You can set which branches are able to be seen by Jenkins under "Branch Sources". The guide
recommended setting the following options:

- Filter by name
- Checkout to matching local branch
- Clean after checkout
- Clean before checkout

In your Jenkinsfile, you can restrict a stage to branches:

```
...
stage('cat README') {
    when {
        branch 'fix-*'
    }

    steps {
        sh 'cat README.md'
    }
}
```

## Using Docker with Pipeline

A simple example:

```Jenkinsfile
/* Requires the Docker Pipeline plugin */
pipeline {
    agent { docker { image 'python:3.10.7-alpine' } }
    stages {
        stage('build') {
            steps {
                sh 'python --version'
            }
        }
    }
}
```

Many build systems cache external dependencies after downloading them for future use. This can
make the Pipeline run faster. We can pass arguments to Docker to take advantage of this:

```Jenkinsfile
/* Requires the Docker Pipeline plugin */
pipeline {
    agent {
        docker {
            image 'python:3.10.7-alpine'
            args: '-v $HOME/.local/bin'
        }
    }

    stages {
        stage('build') {
            steps {
                sh 'python --version'
            }
        }
    }
}
```

You can also specify multiple containers for your Pipeline:

```
pipeline {
    agent none
    stages {
        stage('Back-end') {
            agent {
                docker { image 'maven:3.8.7-eclipse-temurin-11' }
            }
            steps {
                sh 'mvn --version'
            }
        }
        stage('Front-end') {
            agent {
                docker { image 'node:16.13.1-alpine' }
            }
            steps {
                sh 'node --version'
            }
        }
    }
}
```

If you have a custom image you need to use for your application, you can use the Dockerfile within
your repository:

```
pipeline {
    agent { dockerfile true }
    ...
}
```

## Shared Libraries

Shared libraries allow you to use the same code in multiple places in your Jenkins file. Helps save
you from some typing (D.R.Y.) These libraries are often in the form of Git repositories. **Anyone
who is able to push commits could obtain unlimited access to Jenkins**.

All shared libraries have the following structure:

```
(root)
+- src                     # Groovy source files
|   +- org
|       +- foo
|           +- Bar.groovy  # for org.foo.Bar class
+- vars
|   +- foo.groovy          # for global 'foo' variable
|   +- foo.txt             # help for 'foo' variable
+- resources               # resource files (external libraries only)
|   +- org
|       +- foo
|           +- bar.json    # static helper data for org.foo.Bar
```

### Vars

There can be up to 3 top-level folders. The `vars` directory hosts script fiels that are exposed as
variables to the Pipeline. The name of the file is the name of the variable in the Pipeline; the
filename is conventionally *camelCase*.

To call a library in a Jenkinsfile:

```
@Library('my-shared-library') _
```

The ` _` is like `import package.*` in Java.

To add a shared library, go to *Manage Configuration > Global Pipeline Libraries*. Then for
"Retrieval method" choose "Modern SCM" and Git. **Your shared library has to be a Git repository**.

Suppose there is a file `vars/helloWorld.groovy` that contains the following:

```
def call() {
    sh 'echo Hello World!'
}
```

We could then use this in a Jenkinsfile:

```
@Library("shared-library") _

pipeline {
    agent {label "Linux" }

    stages {
        stage('Example') {
            steps {
                helloWorld()
            }
        }
    }
}
```

You can also use parameters:

```
def call(Map map = [:]) {
    sh "echo Hello ${map.name}. Today is ${map.day}!"
}
```

We could then use this in a Jenkinsfile:

```
@Library("shared-library") _

pipeline {
    agent {label "Linux" }

    stages {
        stage('Example') {
            steps {
                helloWorld(day: "Wednesday", name: "Alex")
            }
        }
    }
}
```

### Resources

To request assets from the `resources` folder:

```
def request = libraryResource 'path/to/file'
```

Example scenario:

You want to execute a script

`vars/helloWorldExternal.groovy`:

```
def call(Map config = [:]) {
    loadLinuxScript(name: 'hello-world.sh')
    sh "./hello-world.sh ${config.name} ${config.dayOfWeek}"
}
```

`vars/loadLinuxScript.groovy`:

```
def call(Map config = [:]) {
    def scriptContents = libraryResource "com/example/scripts/${config.name}"

    writeFile file: "${config.name}", text: scriptContents
    sh "chmod a+x ./${config.name}"
}
```

`resources/com/example/scripts/hello-world.sh`:

```
#! /usr/bin/env bash
echo Hello $1. Today is $2.
```

`Jenkinsfile`:

```
@Library("shared-library") _

pipeline {
    agent {label "Linux" }

    stages {
        stage('Example') {
            steps {
                helloWorldExternal(day: "Wednesday", name: "Alex")
            }
        }
    }
}
```

You can go very far with shared-libraries, creating complex classes etc. See towards the bottom of
https://www.jenkins.io/doc/book/pipeline/shared-libraries/ for details.

## Pipeline Development Tools

Jenkins can validate a declarative Pipeline Jenkinsfile before deploying it. The recommended way to
do this is to run the Jenkins linter via a SSH interface:

```
ssh -p $JENKINS_PORT $JENKINS_HOST declarative-linter < Jenkinsfile
```

We can also replay Pipeline runs so we can test different parameters. For instance, in your Jenkinsfile,
you may want to change from Python 3.10 to 3.11 and see how that affects your Pipeline. Just click on the
Pipeline and there should be a "Replay" option. Changes will not be automatically saved so bring them over
if you want to keep them.

## Pipeline Syntax

VERY USEFUL PAGE: https://www.jenkins.io/doc/book/pipeline/syntax/

The time it takes to allocate the top-level agent in a Jenkinsfile happens before any timeout options apply.
This means if you have an agent takes forever, your Pipeline will too.

In a stage, the `options` scope is invoked before any agents AND before checking any `when` conditions.

```
pipeline {
    agent none
    stages {
        stage('Example') {
            agent any
            options {
                // Timeout counter starts BEFORE agent is allocated
                timeout(time: 1, unit: 'SECONDS')
            }
            steps {
                echo 'Hello World'
            }
        }
    }
}
```

Parameters for `agent`:

- `any` - Execute the Pipeline/stage on any available agent
- `none` - Can only be set to the top-level agent. Requires all stages to have their own defined environment
- `label` - Specify which Jenkins environment to execute the Pipeline on
- `docker` - Execute the Pipeline/stage in the defined container (provisioned dynamically)

`docker` can also pull down Docker containers from a Docker repository (e.g. hub.docker.com):

```
agent {
    docker {
        image 'myregistry.com/node'
        label 'my-defined-label'
        registryUrl 'https://myregistry.com/'
        registryCredentialsId 'myPredefinedCredentialsInJenkins'
    }
}
```

### Dockerfile

Dockerfiles can be used in Pipelines; the `dockerfile` in the repository is used.

Conventionally, the Dockerfile is in the top directory.

```
// If Dockerfile is in top directory
agent { dockerfile true }

// If Dockerfile is somewhere else
agent {
    dockerfile {
        filename '...'              // If your Dockerfile is named something else, put it here
        dir '/path/to/Dockerfile'

        // Also can pull down Dockerfiles
        registryUrl 'https://myregistry.com/'
        registryCredentialsId 'myPredefinedCredentialsInJenkins'
    }
}
```

### Kubernetes

Pipelines can also be executed on a Kubernetes cluster. Example:

```
agent {
    kubernetes {
        defaultContainer 'kaniko'
        yaml '''
kind: Pod
spec:
  containers:
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug
    imagePullPolicy: Always
    command:
    - sleep
    args:
    - 99d
    volumeMounts:
      - name: aws-secret
        mountPath: /root/.aws/
      - name: docker-registry-config
        mountPath: /kaniko/.docker
  volumes:
    - name: aws-secret
      secret:
        secretName: aws-secret
    - name: docker-registry-config
      configMap:
        name: docker-registry-config
'''
   }
```

### Parallel

There is the option to have a `parallel` section in a stage.

Note a stage must have one and only one of the following: `steps`, `stages`, `parallel`, or `matrix`.

If one of your Pipelines in parallel fails, you can make all of them abort by including the `failFast true`
option within the stage that contains the parallel scope.

```
pipeline {
    agent any
    stages {
        stage('Non-Parallel Stage') {
            steps {
                echo 'This stage will be executed first.'
            }
        }
        stage('Parallel Stage') {
            when {
                branch 'master'
            }
            failFast true
            parallel {
                stage('Branch A') {
                    agent {
                        label "for-branch-a"
                    }
                    steps {
                        echo "On Branch A"
                    }
                }
                stage('Branch B') {
                    agent {
                        label "for-branch-b"
                    }
                    stages {
                        stage('Nested 1') {
                            steps {
                                echo "In stage Nested 1 within Branch C"
                            }
                        }
                        stage('Nested 2') {
                            steps {
                                echo "In stage Nested 2 within Branch C"
                            }
                        }
                    }
                }
            }
        }
    }
}
```

### Matrix

A matrix can be used to run tasks in parallel. For instance, if you wanted to test your application
on various web-browsers.

Note a stage must have one and only one of the following: `steps`, `stages`, `parallel`, or `matrix`.

You can force your matrix cells to fail if a single one of them fails with the `failFast true` option.
This is applied to the stage containing the matrix.

The `matrix` section must inclide an `axes` section and a `stages` section. The `axes` section defines
values for each axis in the matrix.

### Axis

Each `axes` section specifies one or more `axis` directives that contain a `name` and a list of `values`.
All values from each axis are combined with others to produce cells.

Example - One axis with 3 cells:

```
matrix {
    axes {
        axis {
            name 'PLATFORM'
            values 'linux', 'mac', 'windows'
        }
    }
    // ...
}
```

| linux | mac | windows |
| ----- | --- | ------- |

Example - Two axis with 12 cells:

```
matrix {
    axes {
        axis {
            name 'PLATFORM'
            values 'linux', 'mac', 'windows'
        }
        axis {
            name 'BROWSER'
            values 'chrome', 'edge', 'firefox', 'safari'
        }
    }
    // ...
}
```

| linux   | mac     | windows |
| ------- | ------- | ------- |
| Chrome  | Chrome  | Chrome  |
| Edge    | Edge    | Edge    |
| Firefox | Firefox | Firefox |
| Safari  | Safari  | Safari  |


### Excludes (optional)

The `exclude` section follows the `axes` section within a `matrix`. It allows you to specify cells that
should be excluded from the matrix. For example, Mac OS does not support 32-bit binaries:

```
matrix {
    axes {
        axis {
            name 'PLATFORM'
            values 'linux', 'mac', 'windows'
        }
        axis {
            name 'BROWSER'
            values 'chrome', 'edge', 'firefox', 'safari'
        }
        axis {
            name 'ARCHITECTURE'
            values '32-bit', '64-bit'
        }
    }
    excludes {
        // Removes 4 cells
        exclude {
            axis {
                name 'PLATFORM'
                values 'mac'
            }
            axis {
                name 'ARCHITECTURE'
                values '32-bit'
            }
        }

        // Removes Safari under all platforms but Mac OS
        exclude {
            axis {
                name 'PLATFORM'
                notValue 'macos'
            }
            axis {
                name 'BROWSER'
                notValues 'safari'
            }
        }
    }
    // ...
}
```

## Pipeline Best Practices

### Shell Scripts in Jenkins Pipeline

There are 2 ways you can implement scripts in your Jenkins pipeline:

1. Placing shell scripts on the agent

   `/home/$USER/scripts/hello.sh`:

   ```
   #! /usr/bin/env bash
   echo "Hello World"
   ```

   Then in the Jenkinsfile...

   ```
   ...
   steps {
        sh '/home/$USER/scripts/hello.sh
   }
   ...
   ```

2. Keeping shell scripts in your SCM (e.g. Github)

### Repetition

Avoid writing the same code multiple times (D.R.Y.) by creating a function or step. \
When you are using `sh` and `echo` multiple times in a row, it is more efficient to combine
them into a single command rather than making multiple calls.

### Cleaning up Old Jenkins Builds

Having too many Jenkins builds laying around can slow down your agents and take up disk space.
It is a good idea to have some sort of way to discard old builds but **make sure your users know
about these settings**.

One way you can discard builds is by going to `{project} > Pipeline Syntax > Declarative Directive Generator`
and select `options: Options` then `Build Discarder`. Enter values you like.

Builds that are locked (explicitly told to be kept) will always remain and not be part of the number of jobs
to keep (e.g. you want to keep the last 3 jobs but lock 1. Eventually you will have 4 builds saved)


### Libraries and Variables

- Avoid using Global Variables whenever possible because they take up memory
- Avoid using massive shared libraries because they need to be loaded and can increase Pipeline time

### Avoid Concurrency in Pipelines

Try not to share workspaces across multiple Pipeline executions as one pipeline may modify another file
that is being used by another pipeline.

Ideally, if 2 pipelines need to use the same disk/volume, copy the required files to the pipeline and
copy them back if any modifications were made afterwards.

**This is slower than using unique resources for both jobs**.

## Scaling Pipelines

The most common bottleneck in pipelines is writing transient data to disk *frequently* so running
pipelines can handle unexpected restarts or crashes.

This increases the durability of the pipeline but decreases performance at the same time. Pipelines
can reduce the frequency they write to disk to increase performance with a small impact to durability.
In some instances, Jenkins may not be able to resume after a sudden shutdown.

To enable these settings, users must explicitly enable *Speed/Durability Settings* for pipelines.
There are 3 ways to configure durability settings:

1. **Globally**: located under "Manage Jenkins" > "Configure System", labelled "Pipeline Speed/Durability Settings"
2. **Per pipeline job**: located at the top of the job configuration, labelled "Custom Pipeline Speed/Durability Level".
   This setting will apply the NEXT run.
3. **Per-branch for Multibranch projects**: configure a custom Branch Property Strategy (under SCM) and
   add a property for "Custom Pipeline Speed/Durability Level"

Both 2 and 3 overwrite the global settings. The setting is displayed in the pipeline log.

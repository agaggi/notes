# Using Jenkins

## About Jenkins

A Continuous Integration (CI) took that allows for Continuous Deployment (CD), testing and,
deployment of code pushed to a repository.

No need for "Nightly builds"; code is checked automatically upon each commit (not one massive pull).
If code fails, it is sent back to the developer. If code passes, it goes to the operations team.

Jenkins has hundreds of plugins to customize the application to your needs.

- Maven is also another way to customize how your build server works
- https://www.plugins.jenkins.io for plugins

**Jenkins cannot handle large files and multiple builds.**

## Definitions

**Continuous Integration (CI)** is the practice of automating the integration of code changes from
multiple contributers into a single project.

**Continuous Delivery (CD)** extends upon CI by deploying all code changes to a testing and/or
production environment after building.

CI and CD together form a **pipeline**.

A **controller** is the main instance of Jenkins; stores configurations, plugins, and UIs for
management. \
An **agent** is a machine/container that connects to a Jenkins controller and executes given tasks.

Helpful Links:
- https://www.jenkins.io/doc/book/
- https://www.jenkins.io/doc/book/pipeline/syntax/
- https://www.jenkins.io/doc/book/glossary/
- https://www.jenkins.io/doc/book/troubleshooting/
- https://github.com/jenkinsci/docker/blob/master/README.md

## Credentials

There is a helper function called `credientials()` that allows you to pull in credentials that have
already been pre-defined. The following types of credentials are supported:

- Secret text
- Secret file
- Username and password
- SSH username with private key
- Certificates (PKCS#12 with optional password)
- Docker host certificate authentication credentials

If your credential is not of the above types, you will have to use *with credentials*.

Example:

```Jenkinsfile
...
environment {
    ACCESS_KEY = credentials('credential-id-for-access-key')
}
...
```

Jenkins *uses* the same rules as Groovy for string interpolation. \
**Note: only double-quotes support string interpolation.**

```Jenkinsfile
echo 'Hello ${name}'  ->  Hello ${name}
echo "Hello ${name}"  ->  Hello Alex
```

When using interpolation with credentials, you have to be *very* careful. **Do not use
double-quotes as your credentials will be rendered out**. Jenkins may catch this for you but
don't count on it.

```Jenkinsfile
...
steps {
    stage('Example') {
        /* BAD */
        sh("curl -u ${EXAMPLE_USERNAME}:${EXAMPLE_PASSWORD} https:///example.com")

        /* GOOD */
        sh('curl -u $EXAMPLE_USERNAME:$EXAMPLE_PASSWORD https:///example.com')
    }
}
...
```

**Also be careful of injections (similar to SQL injections)**.

```
...
parameters {
    string(name: 'STATEMENT', defaultValue: 'hello; ls /', description: 'What should I say?')
}

stages {
    stage('Example') {
        steps {
            /* WRONG! */
            sh("echo ${STATEMENT}")

            /* CORRECT */
            sh('echo $STATEMENT')
        }
    }
}
```

When adding credentials, there are 2 types of *scopes* they can be assigned to:

1. Global - if the credential will be used in a Pipeline
2. System - if the credentials will be used for sysadmin functions such as email auth., agent
   connection, etc.

### Usernames and Passwords

There are some types of credentials that will create multiple variables at once:

```
environment {
    BITBUCKET_COMMON_CREDS = credentials('jenkins-bitbucket-common-creds')
}
```

This will create the following variables:

- BITBUCKET_COMMON_CREDS - contains a username and a password separated by a colon in the format
  username:password.
- BITBUCKET_COMMON_CREDS_USR - an additional variable containing the username component only.
- BITBUCKET_COMMON_CREDS_PSW - an additional variable containing the password component only.

### Secret Files

A secret file is a credential that is in a binary format and/or too unwieldy to enter into Jenkins.

Example:

```
pipeline {
    agent any

    environment {
        // Will be assigned the value of a temp variable
        MY_KUBECONFIG = credentials('my-kubeconfig')
    }

    stages {
        stage('Example') {
            steps {
                sh "kubectl --kubeconfig $MY_KUBECONFIG get pods"
            }
        }
    }
}
```

## Searching

The search bar in Jenkins is more powerful than you may think. For example, if you type "foo #53
console", you will be taken to the console output page of the "foo" job build #53. If you are
already inside the foo job, you can just type "#53 console" and it will take you to the same place.

If case-insensitive search is not enabled, you can go to *<your profile>/configure* and find it there.

## Referencing Another Project by Name

If you want to copy artifacts from another project:

```
copyArtifacts projectName: 'myproject'
```

**If you have projects with the same name, you will need to specify the path**.

For other types of projects, refer to:
https://www.jenkins.io/doc/book/using/referencing-another-project-by-name/

## Aborting a build

You can click the X button in the UI or:

- `BUILD ID URL/stop` - aborts a Pipeline.
- `BUILD ID URL/term` - forcibly terminates a build (should only be used if stop does not work).
- `BUILD ID URL/kill` - hard kill a pipeline. This is the most destructive way to stop a pipeline
  and should only be used as a last resort.

If your build is not aborting, check http://yourserver/jenkins/threadDump

## Fingerprints

Jenkins supports **file fingerprinting**, which allows you to track dependencies more easily.

Example:

You have a TOP project that depends on a MIDDLE project that depends on your BOTTOM project. You
fixed a bug reported by the TOP team but they still say it exists. Jenkins can tell you what builds
the TOP and MIDDLE teams are running.

To set this up, all relevant projects need to be configured to *record fingerprints* of jar files.
It is relatively inexpensive to have fingerprinting so it is suggested to take fingerprints of all:

- jar files produced by the project
- jar files the project relies on

To add fingerprinting go to <project>/configure/post-build actions and select "add post-build action".

The fingerprint of a file is its MD5 checksum (actual files are not stored for storage reasons).
These checksums can be seen in `$JENKINS_HOME/fingerprints`.


## Remote Access API

There are currently 3 flavors:

1. XML
2. JSON w/ JSONP support
3. Python

The API is offered in a REST-like style, meaning there is no single entrypoint for all features.
Instead, features are available under the `.../api/` URL where `...` is your Jenkins URL.

What can you do with the API?

- Receive information from Jenkins for programmatic consumption
- Trigger new builds
- Create/copy jobs

See more details here: https://www.jenkins.io/doc/book/using/remote-access-api/

## Executor Starvation

If there is a little black clock next to your process in the build queue, then your job is sitting
in the queue unnecessarily. Some common causes:

1. **Agents are offline** that are required for your build. Go to
   http://server/jenkins/computer/AGENTNAME to understand why, and bring it back online.
2. **Waiting for an available executor on an agent** that is busy and has been waiting longer than
   what the build time actually takes (e.g. wait 5mins for a 2min process). Use labels so builds can
   run on any machine that meet system requirements.
3. **Waiting for an available executor on a label** because the given label is busy. The solution
   is to add more agents.

## Using Agents

An agent is typically a physical machine, virtual machine, or a container. They can also be in the
cloud like Amazon EC2 and Microsoft Azure instances.

It is best practice from both security and performance perspectives to distribute your jobs over
agents rather than running everything on the controller.

To create a Docker agent on Linux:

```
docker run -d --rm --name=agent1 -p 22:22 \
-e "JENKINS_AGENT_SSH_PUBKEY=[your-public-key]" \
jenkins/ssh-agent:alpine
```

**Note**: if port 22 is already being used, specify a different port via `-p [host port]:22`

Then, to activate *agent1* we need to:

1. Go to Manage Jenkins > Manage Cloud and Nodes
2. Click "New Node", enter the name of the agent, and make it permanent
3. Fill in the other fields with desired settings
4. After saving, the agent will be offline until you start it

After all that's done, you can assign jobs to the agent:

- Click on the "Restrict where this project can be run" option
- Enter the label name (e.g. agent1). Be careful with whitespaces
- In the "Build" section, there is an "execute shell" option that can be useful (e.g. echo'ing out
  agent info)

You can mark an agent for maintenance and it will disconnect the agent from Jenkins. You can reconnect
it any time.

## JMeter with Jenkins

https://www.jenkins.io/doc/book/using/using-jmeter-with-jenkins/

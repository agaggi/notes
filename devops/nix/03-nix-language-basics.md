# Nix language basics

## Overview

The Nix language is designed to make creating and composing *derivations* easily. Derivations are
precise descriptions on how contents of already existing files are used to derive new files. 

The Nix language is:

- Domain-specific: it comes with 
  [built-in functions](https://nixos.org/manual/nix/stable/language/builtins) to work with the Nix
  store
- Declarative: there's no notion of executing sequential steps. Dependencies between operations are
  established only if they are data-related
- Pure: **values cannot change during computation**. Functions always produce the same output if
  their input does not change
- Functional: functions are like any other value (first-class function support)
- Lazy: values are only computed when they are needed
- Dynamically typed: type errors are only detected when expressions are evaluated

Notable uses of the Nix language:

- *Nixpkgs*, the largest, most up-to-date software distribution in the world
- *NixOS*, a Linux distribution that can be configured fully declaratively (infrastructure as code)
  and is based on Nix and Nixpkgs

### How to run the examples

#### Interactive evaluation

You can evaluate Nix expressions interactively using `nix repl`:

```
$ nix repl
Welcome to Nix 2.18.1. Type :? for help.

nix-repl> 1 + 2
3
```

> **Note**
>
> Remember Nix is a lazy-evaluated language and `nix repl` only computed values when needed. Some
> examples show a fully-evaluated data structure for clarity but this will likely not be the case
> on your end. If you'd like to see the same output shown, try prepending `:p` to the output
> expression.
>
> ```
> nix-repl> { a.b.c = 1; }
> { a = { ... }; }
>
> nix-repl> :p { a.b.c = 1; }
> { a = { b = { c = 1; }; }; }
> ```
>
> Type `:q` to exit out of `nix repl`.

#### Evaluating Nix files

Use `nix-instantiate --eval` to evaluate expressions in a Nix file.

```
$ echo 1 + 2 > file.nix
$ nix-instantiate --eval file.nix
3
```

> **Note 1**
>
> If no file name is specified, `nix-instantiate` will try to read from `default.nix` by default.

> **Note 2**
>
> Remember the Nix language only computes values when they are needed (lazy evaluation). If you want
> to follow the examples and your output looks different, try adding the `--strict` flag to
> `nix-instantiate`.
>
> ```
> $ echo "{ a.b.c = 1; }" > file.nix
> $ nix-instantiate --eval file.nix
> { a = <CODE>; }
> 
> $ nix-instantiate --eval --strict file.nix
> { a = { b = { c = 1; }; }; }
> ```

## Names and values

### Attribute set

An attribute set is a collection of name-value pairs. Every name must be unique.

If you are familiar with JSON, imagine the Nix language as **JSON with functions**.

<table>
<tr>
<th>Nix</th>
<th>JSON</th>
</tr>
<tr>
<td>

```nix
{
  string = "hello";
  integer = 1;
  float = 3.141;
  bool = true;
  null = null;
  list = [ 1 "two" false ];
  attribute_set = {
    a = "hello";
    b = 2;
    c = 2.718;
    d = false;
  }; # comments are supported
}
```
 </td>
 <td>

 ```json
 {
  "string": "hello",
  "integer": 1,
  "float": 3.141,
  "bool": true,
  "null": null,
  "list": [1, "two", false],
  "object": {
    "a": "hello",
    "b": 1,
    "c": 2.718,
    "d": false
  }
}
```

</td>
</tr>
</table>

### Recursive attribute set

Sometimes you will see attribute sets with `rec` in front of them. This allows for attributes to
access other attributes in the set:

```nix
rec {
  one = 1;
  two = one + 1;
  three = two + 1;
}
```

### `let ... in ...`

Also known as a *let expression* or *let binding*.

`let` expressions allow assigning names to values for repeated use.

```nix
let
    b = a + 1;
    a = 1;
in
a + b
```

Only expressions within the `let` expression itself can access the newly declared variables. You
can think of `let` expressions similarly to scope. For example, the following is invalid:

```nix
{
  a = let x = 1; in x;
  b = x;                # b can't access x!
}
```

### Attribute access

Attributes in a set are accessed with the `.` operator much like how class methods/attributes are
accessed in other languages.

Example:

```nix
let
    attrs = { x = 1 };
in
attrs.x
```

You can also use the `.` operator to assign values to attributes:

```
nix-repl> :p { a.b.c = 1; }
{ a = { b = { c = 1; }; }; }
```

### `with ...; ...`

A `with` expression allows access to attributes without repeatedly referencing the attribute set
they were declared in.

Example:

```nix
let
  a = {
    x = 1;
    y = 2;
    z = 3;
  };
in
with a; [ x y z ]
```

```nix
[ 1 2 3 ]
```

This expression is equivalent to:

```nix
[ a.x a.y a.z ]
```

Remember that attributes made available through `with` are only in scope of the expression that
immediately follows the semi-colon (`;`).

```nix
let
    a = {
        x = 1;
        y = 2;
        z = 3;
    };
in
{
    b = with a; [ x y z ];
    c = x;                    # x is inaccessible!
}
```

### `inherit ...`

If you are bringing a variable into a different scope and for some reason you wanted to use the same
name, `inherit` is a shorthand way to do so:

```nix
let
    x = 1;
    y = 2;
in
{
    inherit x y;        # Equivalent to: x = x; y = y;
}
```

It is also possible to inherit names from a specific attribute set:

```nix
let
  a = { x = 1; y = 2; };
in
{
  inherit (a) x y;      # Equivalent to: x = a.x; y = a.y;
}
```

And `inherit` works inside of `let` expressions too:

```nix
let
    inherit ({ x = 1; y = 2; }) x y;
in [ x y ]
```

The expression above is equivalent to:

```nix
let
    x = { x = 1; y = 2; }.x;
    y = { x = 1; y = 2; }.y;
in [ x y ]
```

### String interpolation

The value of a Nix espression can be inserted into a string with the `${ }` syntax.

Example:

```nix
let
    name = "alex";
in
"Hello ${name}"
```

> **Note**
> 
> It is important to remember string interpolation only works on variables of type string:
> 
> ```nix
> let
>     x = 1;
> in
> "yo ${x}"
> ```
> 
> ```
> error:
>        … while evaluating a path segment
> 
>          at /home/alex/Documents/notes/devops/nix/other/default.nix:4:5:
> 
>             3| in
>             4| "yo ${x}"
>              |     ^
>             5|
> 
>        error: cannot coerce an integer to a string
> ```

### Filesystem paths

Nix supports relative, absolute, and lookup paths. Lookup paths are Nix-specific and are also known
as "angle bracket syntax".

For example, `<nixpkgs>` will usually point to `/nix/var/nix/profiles/per-user/root/channels/nixpkgs`.

The value of a lookup path is a filesystem path that depends on the value of `builtins.nixPath`. The
example, `<nixpkgs>`, will point to a filesystem path of some revision of Nixpkgs.

### Multi-line strings

Multi-line strings are denoted by double-single quotes (`'' ''`).

Example:

```nix
''
  one
   two
    three
''
```

The above string is equivalent to `one\n two\n  three`.

## Functions

**A function always takes exactly one argument**. The argument and function body are separated by a
colon (`:`). The left of the colon is the argument and the right is the body.

Function arguments are the third way, apart from attribute sets and `let` expressions, to assign
names to values.

Function declarations can appear in different forms:

- Single argument
  
  ```nix
  x: x + 1
  ```

  - Multiple arguments (via nesting)

    ```nix
    x: y: x + y
    ```

- Attribute set argument
  
  ```nix
  { a, b }: a + b
  ```

  - With default attributes

    ```nix
    { a, b ? 0 }: a + b
    ```

  - With additional attributes allowed

    ```nix
    { a, b, ... }: a + b
    ```

- Named attribute set argument

  ```nix
  args@{ a, b, ... }: a + b + args.c
  ```

Functions have no names; therefore, they are called *anonymous functions* or a *lambda* function.

```
nix-repl> x: x + 1
«lambda @ «string»:1:1»
```

### Calling functions

When you call a function in Nix, you pass the argument directly after the function's name.

Example:

```nix
let 
    f = x: x + 1;
in f 1
```

#### Multiple arguments

The function `x: y: x + y` is equivalent to `x: (y: x + y)`.

We can call the function like so:

```nix
let
    f = x: y: x + y;
in f 1 2
```

#### Attribute set argument

Nix functions can be declared to require an attribute set with a specific structure as an argument.
The required structure is determined by attribute names separated by commas and enclosed in braces.

Example:

```nix
let
    f = {a, b}: a + b;
in f { a = 5; b = 2; }
```

If your attribute set's structure does not match the requirements of the function, you will get an
error.

Example:

```nix
let
    f = {a, b}: a + b;
in f { a = 1; b = 2; c = 3; }
```

```
    2|     f = {a, b}: a + b;
    3| in f { a = 1; b = 2; c = 3; }
     |    ^
    4|

error: function 'f' called with unexpected argument 'c'
```

> **Note**
> 
> If you want the function to work with aditional arguments but not use them, add `...` after `b`.

#### Default values

Default values are denoted by having an attribute name followed by a `?` then a value.

Example:

```nix
let
    f = {a, b ? 0}: a + b;
in f = { a = 5; }
```

#### Named attribute set argument

An attribute set can be given a name so additional attributes inside of it can be accessible.

Example:

```nix
let
    f = {a, b, ...}@args: a + b + args.c;
in f { a = 1; b = 2; c = 3; }
```

## Function libraries

In addition to the built-in operators (`+`, `==`, `&&`, etc.), there are two widely used libraries
that *together* can be considered a standard for the Nix language. You will need to understand both
to have a good understanding of Nix code.

For reference, here are a few operators that differ from other languages:

| Name                 | Syntax                             | Example                                  |
| -------------------- | ---------------------------------- | ---------------------------------------- |
| Attribute selection  | *attrset*`.`attrpath [`or` *expr*] | `{ a = "Foo"; b = "Bar"; }.c or "Xyzzy"` |
| Function application | *func expr*                        | [See here](#functions)                   |
| Has attribute        | *attrset* `?` *attrpath*           | `{ x, y ? "foo", z ? "bar" }: z + y + x` |
| List concatenation   | *list* `++` *list*                 | `[ "a" "b" "c" ] ++ [ 1 2 3 ]`           |
| Update               | *attrset1* `//` *attrset2*         | `{ a = "Foo" } // { a = "Bar" }`         |
| Logical implication  | *bool* `->` *bool*                 | `true -> false` or `!true \|\| false`    |

### `builtins`

A.K.A. "primative operations" or "primops".

Nix comes with many [built-in functions](https://nixos.org/manual/nix/stable/language/builtins) to 
work with the Nix store and are available under the `builtins` constant:

```
nix-repl> builtins.toString
«primop»
```

Most built-in functions are only accessible through the `builtin` contant; however, `import` is an
exception. It takes a path to a Nix file and reads it to evaluate the contained Nix expression. If
the path points to a directory, Nix will look for a `default.nix` file inside it.

Example:

```
$ echo "x: x + 1" > file.nix
$ nix repl

nix-repl> import ./file.nix 2
3
```

### `pkgs.lib`

The `nixpkgs` repository contains an attribute set called `lib` which provides a large number of
useful functions. These functions are accessed through `pkgs.lib` and are implemented in the Nix
language.

```nix
let
    pkgs = import <nixpkgs> {};
in
pkgs.lib.strings.toUpper "hello"
```

```
HELLO
```

For historical reasons, some functions in `pkgs.lib` are equivalent to functions in `builtin`.

## Impurities

So far we've only talked about *pure functions* which declare data and transform it with functions.
Usually, however, creating derivations involves observing the outside world.

There is one common impurity in Nix and that's reading files from the filesystem as *build inputs*.
Build inputs are files derivations refer to to create new derivations and can be specified by
providing a filesystem path or a dedicated function. 

### Paths

A path must contain **at least one slash** to be recognized as one. Relative and absolute paths are
supported and `~` will evaluate to `/home/$USER`.

Whenever a filesystem path is used in string interpolation, the contents of that file are copied to
the Nix store.

Evaluating `"${./foo.txt}"`, for example, will cause `foo.txt` in the current directory to be copied 
into the Nix store resulting in `/nix/store/<hash>-foo.txt` being created.

The same concept applies for directories.

### Fetchers

Files can be used as build inputs but they do not have to come from the filesystem. The Nix language
provides built-in impure functions to fetch files over networks:

- `builtins.fetchurl`
- `builtins.fetchTarball`
- `builtins.fetchGit`
- `builtins.fetchClosure`

Some functions provide extra convenience like unpacking archives. All of them, however, will produce
a filesystem path in the Nix store.

Example:

```
builtins.fetchTarball "https://github.com/NixOS/nix/archive/7c3ab5751568a0bc63430b33a5169c5e4784a0ff.tar.gz"
```

```
"/nix/store/d59llm96vgis5fy231x6m7nrijs0ww36-source"
```

## Derivations

Derivations are the core of both Nix and the Nix language:

- The Nix language is used to describe derivations
- Nix runs derivations to produce *build results*
- Build results can then be used as inputs to other derivations

The way derivatives are declared in the Nix language is by using the built-in impure function,
`derivation`. It's usually wrapped by the Nixpkgs build mechanism `stdenv.mkDerivation`; it
simplifies the build process greatly.

> **Note**
> 
> You will likely never encounter `derivation` in practice.



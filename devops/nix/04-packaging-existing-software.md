# Packaging existing software with Nix

One of Nix's primary use-cases is addressing common difficulties encountered with packaging software
such as specifying and obtaining dependencies.

This document serves as a tutorial for someone looking to create their first Nix derivation. The
example involves packaging C/C++ software and uses the
[Nixpkgs Standard Environment](https://nixos.org/manual/nixpkgs/stable/#part-stdenv) (`stdenv`) 
which automates much of the packaging process.

## Your first package

A package is a loosely defined concept that refers to either a collection of files and other data,
or a Nix expression representing such a collection before it comes into being.

Packages in Nixpkgs have a conventional structure that allows them to be searched for and integrated
into environments alongside other packages.

Below is an extremely simple function that takes an attribute set containing `stdenv` and produces a
derivation (which is currently nothing):

```nix
{ stdenv }:

stdenv.mkDerivation {};
```

### Package functions

In this example, we'll be looking at creating a derivation based on GNU Hello, a simple program that
outputs "hello world". It's source code is available [here](https://ftp.gnu.org/gnu/hello/).

To begin, add `pname` and `version` attributes to the set passed to `mkDerivation`. Every package 
is required to have a name and a version.

```nix
{ stdenv }:

stdenv.mkDerivation {
    pname = "hello";
    version = "2.12.1";
};
```

Next, we'll declare a dependency for our derivation on the latest version of `hello`:

```nix
{ lib, stdenv, fetchzip }:

stdenv.mkDerivation {
    pname = "hello";
    version = "2.12.1";

    src = fetchzip {
        url = "https://ftp.gnu.org/gnu/hello/hello-2.12.1.tar.gz";
        sha256 = lib.fakeSha256;
    };
};
```

> **Note 1**
> 
> [This page](https://nixos.org/manual/nixpkgs/stable/#fetchurl) contains useful information on
> the various fetching functions.

> **Note 2**
> 
> It is common practice to initially use `lib.fakeSha256` when building derivations because
> hashes of dependencies may not be able to be determined until downloaded and unpacked. Nix will
> report the hashes after building.

Save the derivation to a file called `hello.nix` and run `nix-build`:

```
$ nix-build hello.nix
error: cannot evaluate a function that has an argument without a value ('lib')
       Nix attempted to evaluate a function as a top level expression; in
       this case it must have its arguments supplied either by default
       values, or passed explicitly with '--arg' or '--argstr'. See
       https://nixos.org/manual/nix/stable/language/constructs.html#functions.

       at /home/alex/Documents/notes/devops/nix/hello.nix:1:3:

            1| { lib, stdenv, fetchzip }:
             |   ^
            2|
```

Our build failed because the expression in `hello.nix` is a *function* that will produce its
intended output **only if it is passed the correct arguments**.

### Building with `nix-build`

`lib` is available from `nixpkgs`, which must be imported using another Nix expression and passed
as an argument to our `hello.nix` derivation. The recommended way to do this is to create a file
called `default.nix` in the same directory as `hello.nix` with the following contents:

```nix
let
  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/nixos-23.11";
  pkgs = import nixpkgs { config = {}; overlays = []; };
in
{
  hello = pkgs.callPackage ./hello.nix {};
}
```

> **Note**
> 
> `callPackage` automatically passes attributes from `pkgs` to the provided function. In this case,
> `callPackage` will supply `lib`, `stdenv`, and `fetchzip` to the function defined in `hello.nix`.

By having this file, you can run `nix-build -A hello` to build the derivation in `hello.nix`.

```
$ nix-build -A hello
...
unpacking source archive /build/hello-2.12.1.tar.gz
error: hash mismatch in fixed-output derivation '/nix/store/axarych0fz65cqgmxa8vcy8gf6k2ac0w-source.drv':
         specified: sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=
            got:    sha256-1kJjhtlsAkpNB7f6tZEs+dbKd8z7KoNHyDHEJ0tmhnc=
error: 1 dependencies of derivation '/nix/store/1v7bpw8z0kqlmqizyx82hvgmzrgdh6rx-hello-2.12.1.drv' failed to build
```

The last thing we will need to fix is the default SHA256 hash we set in the `fetchzip` function to
the one Nix retrieved. Our final `hello.nix` file will look like this:

```nix
{ stdenv, fetchzip }:

stdenv.mkDerivation {
    pname = "hello";
    version = "2.12.1";

    src = fetchzip {
        url = "https://ftp.gnu.org/gnu/hello/hello-2.12.1.tar.gz";
        sha256 = "1kJjhtlsAkpNB7f6tZEs+dbKd8z7KoNHyDHEJ0tmhnc=";
    };
};
```

### Build result

Upon successful completion, Nix will give you the path in the Nix store of your newly built package.
Additionally, in the directory where you performed the build, a `result` directory will have been
created. It's simply a symlink to the Nix store path of your build.

```
$ ls -al
...
lrwxrwxrwx 1 alex users   56 Mar 24 11:46 result -> /nix/store/ra5dpfzdbrfrz5wqlvb68q7g598c3mk6-hello-2.12.1
...
```

## Packages with dependencies

Now let's say we want a more complicated program such as `icat`, a program that outputs images in
the terminal.

Change `default.nix` to include a new attribute for `icat`:

```nix
let
  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/nixos-23.11";
  pkgs = import nixpkgs { config = {}; overlays = []; };
in
{
  hello = pkgs.callPackage ./hello.nix {};
  icat = pkgs.callPackage ./icat.nix {};
}
```

Then, make a copy of `hello.nix` and name it `icat.nix`; adjust `pname` and `version` accordingly.

### Fetching a source from GitHub

The source code for `icat` is available on GitHub (see https://github.com/atextor/icat). On the
GitHub page, navigate to the tags; these are the versions you can choose to download.

Because we will be fetching from a URL, we can use `nix-prefetch-url` to get the hash immediately
instead of using `lib.fakeSha256`. This could have been done in the first example as well.

```
$ nix-prefetch-url --unpack https://github.com/atextor/icat/archive/refs/tags/v0.5.tar.gz --type sha256

path is '/nix/store/p8jl1jlqxcsc7ryiazbpm7c1mqb6848b-v0.5.tar.gz'
0wyy2ksxp95vnh71ybj1bbmqd5ggp13x3mk37pzr99ljs9awy8ka
```

With the hash, we have all of the basic information for our derivation.

> **Note**
> 
> Nix comes with a `fetchFromGitHub` function we can use in place of `fetchzip`.

```nix
{ lib, stdenv, fetchFromGitHub }:

stdenv.mkDerivation {
    pname = "icat";
    version = "v0.5";

    src = fetchFromGitHub {
        owner = "atextor";
        repo = "icat";
        rev = "v0.5";
        sha256 = "0wyy2ksxp95vnh71ybj1bbmqd5ggp13x3mk37pzr99ljs9awy8ka";
    };
}
```

### Missing dependencies

Try building the `icat` derivation and you will encounter a compiler error:

```
$ nix-build -A icat
these 2 derivations will be built:
  /nix/store/wvc8xa00iz8gbszrm1sjvji8bz0r8pkf-source.drv
  /nix/store/6vzs4xg9s6hvd6znlvh9sn0l3569ibn3-icat-v0.5.drv
...
error: builder for '/nix/store/6vzs4xg9s6hvd6znlvh9sn0l3569ibn3-icat-v0.5.drv' failed with exit code 2;
       last 10 log lines:
       >                  from /nix/store/iczlqwdj10xiz3mmms6adnra0mn3ljk1-glibc-2.35-224-dev/include/stdio.h:27,
       >                  from icat.c:31:
       > /nix/store/iczlqwdj10xiz3mmms6adnra0mn3ljk1-glibc-2.35-224-dev/include/features.h:194:3: warning: #warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE" [8;;https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wcpp-Wcpp8;;]
       >   194 | # warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE"
       >       |   ^~~~~~~
       > icat.c:39:10: fatal error: Imlib2.h: No such file or directory
       >    39 | #include <Imlib2.h>
       >       |          ^~~~~~~~~~
       > compilation terminated.
       > make: *** [Makefile:16: icat.o] Error 1
       For full logs, run 'nix log /nix/store/6vzs4xg9s6hvd6znlvh9sn0l3569ibn3-icat-v0.5.drv'.
```

From the error above, we can see there is a library called `Imlib2` that is missing. We can search
for it on https://search.nixos.org/packages and add it to our derivation:

```nix
{ lib, stdenv, fetchFromGitHub, imlib2 }:

stdenv.mkDerivation {
    pname = "icat";
    version = "v0.5";

    src = fetchFromGitHub {
        owner = "atextor";
        repo = "icat";
        rev = "v0.5";
        sha256 = "0wyy2ksxp95vnh71ybj1bbmqd5ggp13x3mk37pzr99ljs9awy8ka";
    };

    buildInputs = [ imlib2 ];
}
```

If we try to build our derivation again, it will proceed further but we will encounter another error
dependency-related error.

```
$ nix-build -A icat
this derivation will be built:
  /nix/store/98mvlyyslclvyvdb10knng1pjdcf6m2q-icat-v0.5.drv
...
error: builder for '/nix/store/98mvlyyslclvyvdb10knng1pjdcf6m2q-icat-v0.5.drv' failed with exit code 2;
       last 10 log lines:
       >                  from icat.c:31:
       > /nix/store/iczlqwdj10xiz3mmms6adnra0mn3ljk1-glibc-2.35-224-dev/include/features.h:194:3: warning: #warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE" [8;;https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wcpp-Wcpp8;;]
       >   194 | # warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE"
       >       |   ^~~~~~~
       > In file included from icat.c:39:
       > /nix/store/0fvlwlpk3jblkh3ny4dy87nz5pq7fchx-imlib2-1.9.1-dev/include/Imlib2.h:45:10: fatal error: X11/Xlib.h: No such file or directory
       >    45 | #include <X11/Xlib.h>
       >       |          ^~~~~~~~~~~~
       > compilation terminated.
       > make: *** [Makefile:16: icat.o] Error 1
       For full logs, run 'nix log /nix/store/98mvlyyslclvyvdb10knng1pjdcf6m2q-icat-v0.5.drv'.
```

## Finding packages

Determining from where to source a dependency can be a somewhat involved process because package
names don't always correspond to library or program names.

In the example above, we got a dependency error that said the `Xlib.h` headers from the `X11` C
package are missing. There are multiple ways to figure out what package we need:

### NixOS package search

We can go to https://search.nixos.org/packages and search for the names of packages we are looking
for. Unfortunately, in this situation, searching for `Xlib` produces a lot of irrelevant results.

### Using `git` and `rg`

To find name assignments in a package source, we can search for `"<keyword> ="`.

If you don't already have *Git* and *Ripgrep* installed you can use `nix-shell` to temporarily do
so. Then we can clone the `nixpkgs` Git repository to perform a search:

```
$ nix-shell -p git ripgrep
[nix-shell:~]$ git clone https://github.com/NixOS/nixpkgs --depth 1
[nix-shell:~]$ rg -i "libx11 =" nixpkgs/pkgs
nixpkgs/pkgs/top-level/all-packages.nix
14043:    libX11 = xorg.libX11;

nixpkgs/pkgs/applications/version-management/monotone-viz/graphviz-2.0.nix
55:    ++ lib.optional (libX11 == null) "--without-x";

nixpkgs/pkgs/servers/x11/xorg/overrides.nix
147:  libX11 = super.libX11.overrideAttrs (attrs: {

nixpkgs/pkgs/servers/x11/xorg/default.nix
1119:  libX11 = callPackage ({ stdenv, pkg-config, fetchurl, xorgproto, libpthreadstubs, libxcb, xtrans, testers }: stdenv.mkDerivation (finalAttrs: {
```

> **Note**
> 
> Ripgrep is case-sensitive by default. Use the `-i` flag for a case-insensitive search.

## Fixing build failures

Based on our search, the package we are looking for seems to be `xorg.libX11`. We can add this to
our derivation:

```nix
{ lib, stdenv, fetchFromGitHub, imlib2, xorg }:

stdenv.mkDerivation {
    pname = "icat";
    version = "v0.5";

    src = fetchFromGitHub {
        owner = "atextor";
        repo = "icat";
        rev = "v0.5";
        sha256 = "0wyy2ksxp95vnh71ybj1bbmqd5ggp13x3mk37pzr99ljs9awy8ka";
    };

    buildInputs = [ imlib2 xorg.libX11 ];
}
```

Let's try building again:

```
$ nix-build -A icat
these 2 derivations will be built:
  /nix/store/wvc8xa00iz8gbszrm1sjvji8bz0r8pkf-source.drv
  /nix/store/8d09yzf5fp334qq3lq45irn8156w7r7w-icat-v0.5.drv
...
error: builder for '/nix/store/8d09yzf5fp334qq3lq45irn8156w7r7w-icat-v0.5.drv' failed with exit code 2;
       last 10 log lines:
       >   194 | # warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE"
       >       |   ^~~~~~~
       > icat.c: In function 'main':
       > icat.c:319:33: warning: ignoring return value of 'write' declared with attribute 'warn_unused_result' [8;;https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wunused-result-Wunused-result8;;]
       >   319 |                                 write(tempfile, &buf, 1);
       >       |                                 ^~~~~~~~~~~~~~~~~~~~~~~~
       > gcc -o icat icat.o -lImlib2
       > installing
       > install flags: SHELL=/nix/store/kga2r02rmyxl14sg96nxbdhifq3rb8lc-bash-5.1-p16/bin/bash install
       > make: *** No rule to make target 'install'.  Stop.
       For full logs, run 'nix log /nix/store/8d09yzf5fp334qq3lq45irn8156w7r7w-icat-v0.5.drv'.
```

Notice the message `make: *** No rule to make target 'install'.  Stop.`

### `installPhase`

`stdenv` is automatically working with the `Makefile` included with `icat`. The output shows that
`icat` is building successfully; however, fails when running `make install`. This is because the
Makefile included in the project repository not having an `install` target.

If we look at the [repository for `icat`](https://github.com/atextor/icat), the `README.md` file
only mentions building the program using `make` and leaves *installing* the software up to the users
(i.e. adding the binary to your `PATH`).

We can add an `install` target to our derivation by using the `installPhase` attribute. It contains
a number of commands that are executed to perform the installation.

Because `make` finishes properly, there will be an `icat` executable available in the build
directory. **You simply need to copy the binary to the output directory**. In Nix, the output
directory is stored in the `$out` variable and is accessible in the derivation's [`builder`
execution environment](https://nix.dev/manual/nix/2.19/language/derivations#builder-execution).

```nix
{ lib, stdenv, fetchFromGitHub, imlib2, xorg }:

stdenv.mkDerivation {
    pname = "icat";
    version = "v0.5";

    src = fetchFromGitHub {
        owner = "atextor";
        repo = "icat";
        rev = "v0.5";
        sha256 = "0wyy2ksxp95vnh71ybj1bbmqd5ggp13x3mk37pzr99ljs9awy8ka";
    };

    buildInputs = [ imlib2 xorg.libX11 ];

    installPhase = ''
        runHook preInstall
        mkdir -p $out/bin
        cp icat $out/bin
        runHook postInstall
    '';
}
```

> **Note**
> 
> For details on `runHook`, see the section below.

### Phases and hooks

A Nixpkgs `stdenv.mkDerivation` is separated into 
[phases](https://nixos.org/manual/nixpkgs/stable/#sec-stdenv-phases), each controlling some aspect
of the build process.

Earlier we found out `stdenv.mkDerivation` expected the `Makefile` included in the `icat` project
to have an install target. As a result, we had to define a custom `installPhase` that included
instructions on where to put the built binary.

When building derivations, there are a number of shell functions called *hooks* which can do things
such as set variables, source files, create directories, and so on. Hooks are specific to each phase
and run before and after a phase's execution. They modify the build environment for common
operations during the build.

You may have noticed the `runHook` commands included in the `installPhase` of our derivation above.

It's good practice to include hooks in custom derivation phases because they allow for
[overriding](https://nixos.org/manual/nixpkgs/stable/#chap-overrides) specific parts of the
derivation that you or users may need to do in the future for whatever reason.

## Finally, a successful build

Try running your derivation with the `installPhase` included and you will notice an `icat`
executable will be placed under `results/bin`.

# Introduction

Nix is a **purely functional package manager**. This means packages are treated like values in
functional programming languages such as [Haskell](https://www.haskell.org/). Packages are built by
functions that do not have side-effects and never change after they are built.

Nix stores packages in the *Nix store*, usually located at `/nix/store`, where each package has
its own unique sub-directory. For example:

```
/nix/store/ncwrw7hqm21qxpdcm2yysm8a1h8gww3j-firefox-119.0/
```

Each sub-directory has a unique cryptographic hash such as `ncwrw7hqm21...` shown above.

## Multiple versions of packages

You can have multiple versions or variants of packages installed at the same time without having
them interfere with each other. This is thanks to the hashing scheme shown above. 

One important aspect of the Nix package manager is that upgrading or uninstalling an application
cannot break other applications. This is because these operations never "destructively" update or
delete files that are used by other packages. For example, if a new version of a package is 
required, say Python 3.12, but another installed package requires Python 3.11, a Nix store 
sub-directory will be created for Python 3.12 while the Python 3.11 package remains unmodified in
any way.

## Complete dependencies

Nix ensures package dependency specifications are complete. 

When you are making a package for a package management system on traditional Linux distributions,
such as `apt` for Ubuntu, you need to specify all of the dependencies. Even when done, there are no
guarentees the your specification is complete. You may have forgotten to add a package to your
requirements so, when you build and run your package, you may encounter a "well it works on my
machine" scenario.

Nix on the otherhand does not install packages in global locations such as `/usr/bin` but rather in
package-specific directories under `/nix/store`. This greatly reduces the risk of having incomplete
dependencies.

For example, when you are building your package, you are forced to specify each dependency
explicitly because tools such as compilers don't search in per-package directories such as
`/5lbfaxb722zp...-openssl-0.9.8d/include`. So, if a package builds on your system, that means you
specified all dependencies explicitly.

Once a package is built, runtime dependencies are found using the hash of its Nix store path
(e.g. `5lbfaxb722zp...`).

## Multi-user support

Nix has multi-user support, meaning you do not have to be root to securely install software. Each
user can have a different *profile*, a set of Nix store packages that appear on the user's `PATH`.
If a user installs a package that is already installed by another user, it won't be built or
downloaded again.

## Atomic upgrades and rollbacks

When different versions of the same package are installed, they are added to different paths in the
Nix store. Package management operations **never** overwrite already existing packages when newer
versions are being installed because there may be a window where apps can crash because they are
using the older version of the package still. This makes the Nix package manager **atomic**.

Additionally, since packages aren't overwritten, the old versions of packages are still available
in case you need to roll back to your previous state.

```
$ nix-env --upgrade --attr nixpkgs.some-package
$ nix-env --rollback
```

## Garbage collection

When you uninstall a package like this:

```
$ nix-env --uninstall firefox
```

The package isn't deleted from the system right away in case you want to roll back or it is installed
in another user's profile. Instead, unused packages can be deleted safely by running the **garbage
collector**:

```
$ nix-collect-garbage
```

This deletes all packages that aren't in any user's profile or being used by a currently running
program.

## Functional package language

Packages are built from *Nix expressions*, which is a simple functional language. Nix expressions
describe everything that goes into a package build task (called a *derivation*):

- Other packages
- Sources (e.g. Git repositories)
- Build scripts
- Environmental variables for build scripts
- etc.

Nix tries hard to ensure that Nix expressions are **deterministic**, meaning building a Nix
expression twice should yield the exact same result.

Additionally, because Nix is a functional language, it's easy to build multiple variations of the
same package. You can do things like turn a Nix expression into a function and call it any number of
times with different arguments. Again, due to Nix's hashing scheme, the multiple different versions
don't conflict with each other in the Nix store.

## Transparent source/binary deployment

Nix expressions describe how to build a package from source, so an installation action such as:

```
$ nix-env --install --attr nixpkgs.firefox
```

could take quite a long time since not only firefox, but all of its dependencies (all the way down
to the C libraries and compiler) would need to have been built if they are not already in the Nix
store. This is a **source deployment model**.

For most users, building from source is not ideal. Nix, however, can skip this process and use a
**binary cache**, a web-server providing pre-built binaries. For example, if Nix is asked to build
`nix/store/b6gvzjyb2pg0...-firefox-33.1` from source, it will check if the file
`https://cache.nixos.org/b6gvzjyb2pg0….narinfo` exists in the binary cache. If so, the pre-built
binary is fetched instead of building from source.

## Managing build environments

Nix is useful for developers because it makes it easy to automatically set up the build environment
for a package. Given a Nix expression that describes the dependencies of your package, the
`nix-shell` command will build or download those dependencies and start a Bash shell that contains
all necessary environmental variables (such as compiler search paths) are set.

For example, the following command gets all of the dependencies for the Pan newsreader described in
its Nix expression:

```
$ nix-shell '<nixpkgs>' --attr pan
```

Then you are dropped into a shell where you can edit, build, and test the package:

```
[nix-shell]$ unpackPhase
[nix-shell]$ cd pan-*
[nix-shell]$ configurePhase
[nix-shell]$ buildPhase
[nix-shell]$ ./pan/gui/pan
```

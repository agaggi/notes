# The Nix store

The *Nix store* is an abstraction used to store immutable filesystem data (i.e. packages) that can
have dependencies on other data.

## Filesystem objects

In the Nix store, each package will have a simple filesystem that can contain files, directories,
and/or symbolic links.

Example:

```
├── bin
│   └── hello: 35 KB, executable: true
└── share
    ├── info
    │   └── hello.info: 36 KB, executable: false
    └── man
        └── man1
            └── hello.1.gz: 790 B, executable: false
```

## Store objects

A Nix store is a collection of *store objects* with references between them. A store object consists
of:

- A filesystem object 
- A set of store paths that represent references to other store objects

Store objects are immutable, meaning they do not change until they are deleted.

## Store paths

Nix implements references through the use of *store paths* that serve as unique identifiers. Store
paths consist of a 20-byte digest for identification and a symbolic name for people to read.

For example,

- Digest: `b6gvzjyb2pg0kjfwrjmg1vfhh54ad73z`
- Name: `firefox 33.1`

Putting it all together we get:

```
  /nix/store/b6gvzjyb2pg0kjfwrjmg1vfhh54ad73z-firefox-33.1
  |--------| |------------------------------| |----------|
store directory            digest                 name
```

## Store directories

Every Nix store has a *store directory*. The most common known is `/nix/store`.

Not every store can be accessed through the filesystem but if the store has a filesystem
representation, then the store directory contains filesystem objects that can be addressed using
store paths.

# Package parameters and overrides with `callPackage`

Nixpkgs has coding conventions and idioms that have emerged over the years. They involve composing
parameterised packages with automatic settings through the use of the `callPackage` function.

## Automatic function calls

We will be creating a package that follows standard conventions. Nixpkgs will typically contain the
following:

- A function that takes an attribute set
- Attributes that correspond to derivations in the top-level package set
- A derivation that will eventually be returned

Create a new file called `hello.nix`:

```nix
{ writeShellScriptBin }:
writeShellScriptBin "hello" ''
  echo "Hello, world!"
''
```

> **Detailed explanation**
> 
> `hello.nix` declares a function that takes an attribute set as an argument with one element, 
> `writeShellScriptBin`, which is a Nixpkgs 
> [build helper](https://nixos.org/manual/nixpkgs/unstable/#part-builders) function that returns a
> derivation. The derivation output will be a shell script stored under `$out/bin/hello` that prints
> "Hello world" when run.

Then create a `default.nix` file containing the following:

```nix
let
  pkgs = import <nixpkgs> { };
in
pkgs.callPackage ./hello.nix { }
```

You can now run `nix-build` and your derivation will be built. Check your current directory for a
`results` folder; it will contain your binary.

For every attribute in the function's argument in `hello.nix`, `callPackage` from `default.nix` will
pass attributes from the `pkgs` attribute set if they exist.

## Parameterised builds

Change `default.nix` to produce an attribute set of derivations with an attribute, `hello`,
containing the derivation we made above:

```nix
let
  pkgs = import <nixpkgs> { };
in 
{
  hello = pkgs.callPackage ./hello.nix { };
}
```

When building our `hello` derivation now, we'll need to specify we want to build it by using the
name of the attribute in our set inside of `default.nix` we defined above (which was `hello`). We
can do this with the following command:

```
$ nix-build -A hello
$ ./result/bin/hello
Hello, world!
```

Also, change `hello.nix` by adding an additional parameter, `audience` that defaults to "world":

```nix
{ writeShellScriptBin, audience ? "world" }:

writeShellScriptBin "hello" ''
  echo "Hello, ${audience}!"
''
```

Then let's go into `default.nix` and pass in an argument:

```nix
let
  pkgs = import <nixpkgs> { };
in 
{
  hello = pkgs.callPackage ./hello.nix { audience = "people"; };
}
```

```
$ nix-build -A hello
...
$ ./result/bin/hello
Hello, people!
```

## Overrides

`callPackage` has a convenient feature by allowing parameters to be customized after a derivation
is declared and implemented.

For example:

```nix
let
  pkgs = import <nixpkgs> { };
in 
rec {
  hello = pkgs.callPackage ./hello.nix { audience = "people"; };
  hello-folks = hello.override { audience = "folks"; };
}
```

> **Note**
> 
> The `rec` keyword is used because we reference the `hello` attribute inside the same attribute set. 

`override` passes the value assigned to the `audience` argument into the original function defined
in `hello.nix`. If you build the `hello-folks` derivation, you will notice that the output changes:

```
$ nix-build -A hello-folks
...
$ ./result/bin/hello
Hello, folks!
```

### Interdependent package sets

You can actually create your own version of `callPackage`. This comes in handy when package sets
depend on each other.

Consider the following recursive set of derivations:

```nix
let
  pkgs = import <nixpkgs> { };
in
rec {
  a = pkgs.callPackage ./a.nix { };
  b = pkgs.callPackage ./b.nix { inherit a; };
  c = pkgs.callPackage ./c.nix { inherit b; };
  d = pkgs.callPackage ./d.nix { };
  e = pkgs.callPackage ./e.nix { inherit c d; };
}
```

> **Note**
>
> `inherit a;` is equivalent to `a = a;` and so on.

Previously declared derivations are passed as arguments to other derivations through `callPackage`.

In the case above, you have to remember to manually specify all arguments required by each package.
If `./b.nix` requires argument `a` and there is no `pkgs.a`, you'll get an error. This can get quite
tedious when working with larger package sets.

Use `lib.callPackageWith` to create your own `callPackage` based on an attribute set:

```nix
let
  pkgs = import <nixpkgs> { };
  callPackage = pkgs.lib.callPackageWith (pkgs // packages);
  packages = {
    a = callPackage ./a.nix { };
    b = callPackage ./b.nix { };
    c = callPackage ./c.nix { };
    d = callPackage ./d.nix { };
    e = callPackage ./e.nix { };
  };
in
packages
```

Explanation:

First, note that instead of a recursive attribute set, the derivations are now assigned to a `let`
binding. `let` bindings have the same feature as recursive sets where names on the left-hand side
can be referenced on the right-hand side (e.g. `./b.nix` inherits `a`).

For this reason we can refer to `packages` when we merge its contents with the pre-existing `pkgs`
attribute set using the `//` operator.

The custom `callPackages` we defined above now makes all of the attributes in `pkgs` *and*
`packages`, which we also defined above, to the called package function (e.g. `a`). The names
defined in `packages` will take precedence over those in `pkgs`.

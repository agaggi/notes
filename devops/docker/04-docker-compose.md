# Docker Compose

Docker Compose is a tool for defining and running multi-container applications.

Compose simplifies the control of your entire application stack, making it easy to manage services,
networks, and volumes in a single, comprehensible YAML configuration file. Then, with a single 
command, you create and start all the services from your configuration file.

## Basics of Docker Compose

The default filename `docker compose` will look for is either `compose.yaml` or `docker-compose.yaml`.
If both exist, `compose.yaml` will be preferred.

Here are some key commands:

| Command                   | Description                                                               |
| ------------------------- | ------------------------------------------------------------------------- |
| `docker compose up`       | Starts services defined in your `compose.yaml` file                       |
| `docker compose up -d`    | Starts services defined in your `compose.yaml` file **in detatched mode** |
| `docker compose down`     | Stops services defined in your `compose.yaml` file                        |
| `docker compose logs`     | Allows you to view logs of running services                               |
| `docker compose ps`       | Lists all services and their current status                               |

For a full list of available commands, see https://docs.docker.com/reference/cli/docker/compose/#subcommands.

> **Note**
> 
> If your compose file is named something other than `compose.yaml` or `docker-compose.yaml`, you will
> need to use the `-f` flag to tell Docker Compose the filename.
> 
> Examples:
> 
> ```
> docker compose -f my-compose.yaml up -d
> ```
> 
> ```
> docker compose -f my-compose.yaml logs --no-color > logs.txt
> ```

## An example `compose.yaml` file

```yaml
services:
  web:
    image: php:8.0-apache
    container_name: php_app
    volumes:
      - ./app:/var/www/html
    ports:
      - "8080:80"
    environment:
      - APACHE_DOCUMENT_ROOT=/var/www/html
    depends_on:
      - db
    networks:
      - webnet

  db:
    image: postgres:16-alpine
    container_name: postgres_db
    environment:
      POSTGRES_USER: user
      POSTGRES_PASSWORD: password
      POSTGRES_DB: mydatabase
    volumes:
      - db_data:/var/lib/postgresql/data
    networks:
      - webnet

volumes:
  db_data:

networks:
  webnet:
    driver: bridge
```

### Breakin' it down

#### Services

The first portion of this `compose.yaml` file is where we define our `services`. 

```yaml
services:
  web:
    ...
  db:
    ...
```

You can think of each service as a definition for a container. This includes arguments that would be
passed as parameters when creating a container via the command-line.

For example, the command below is the same as "creating" the `web` service:

```
docker run \
    --name php_app \
    -v ./app:/var/www/html \
    -p 8080:80 \
    -e APACHE_DOCUMENT_ROOT=/var/www/html \
    --network webnet \
    php:8.0-apache
```

#### Volumes

In the `compose.yaml` file above, there are two types of volumes created:

1. Named volumes: these volumes are explicitly given names and persist *usually* under
   `/var/lib/docker/volumes/`

   ```
   docker volume create db_data
   ```

2. Anonymous volumes: these volumes are created through a `docker run` command with the `-v` flag

   ```
   docker run \
       --name php_app \
       -v ./app:/var/www/html \
       ...
   ```

#### Networks

You can create networks in `compose.yaml` files. The one above is equivalent to the following
`docker` command:

```
docker network create webnet --driver bridge
```

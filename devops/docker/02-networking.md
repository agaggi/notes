# Docker networks

Docker's default network is called `bridge`. If you run the `ip a` command on Linux, it is the 
`docker0` network interface.

`docker0` acts like a network switch and provides your Docker containers a means to connect to the
Internet. Each container gets its own virtual ethernet interface and is assigned an IP address
through DHCP. Additionally, `docker0` acts like a DNS so containers can talk to any other containers
on the same network and connect to the Internet **by default**. 

You can see the list of Docker networks with the following command:

```
$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
ead16d805e16   bridge    bridge    local
eb2665cdc75c   host      host      local
71adfbd3459b   none      null      local
```

> **Note**
>
> The `driver` column states the type of network. We'll see different types of networks below.

## User-defined bridge

The default `bridge` network does its job well, but **it is actually best practice to create your
own network**. These kinds of networks are called *User-defined bridges*. We can create one like so:

```
$ docker network create [NAME]
```

Afterwards, if you run `ip a` again, you will see yet another network interface. Containers in this
network, for example, would not be able to communicate with containers in the default bridge
network unless explicitly stated.

> **Note**
> 
> The name of a network can be specified with the `--network` flag when using `docker run`.

A really useful feature of user-defined bridges (and containers running in the same network in
general) is that containers can ping each other by name if they are in the same network. Container's
IP addresses can change over time so using the names of containers provides more consistency.

## Host network

You can make you container "share" the same network as your host. The container will have the
same IP address and ports as your computer (you will not need to forward any ports).

You can enable this type of network by adding the `--network host` flag and option to `docker run`.

The container essentially leeches off the host and there is **no isolation** between your host
network and the container's network.

## MACVLAN

This network type makes it as if containers are connected directly to the host network. Each
container even gets its own IP and MAC address.

To create the network:

```
$ docker network create -d macvlan \
    --subnet [SUBNET]                   # e.g. 192.168.1.0/24 
    --gateway [ROUTER_GATEWAY]          # e.g. 192.168.1.1
    -o parent=[NETWORK_INTERFACE]       # e.g. enp0s6
    [NAME]
```

To assign a container to the network:

```
$ docker run -itd --network [NETWORK_NAME] --ip [CUSTOM_IP] --name [CONTAINER_NAME] [IMAGE_NAME]
```

An issue you may face with MACVLAN networks is the inability to ping/connect to anything on your
network. This is caused by having multiple MAC addreses (one for host + one for each container on 
the network) per IP address; your switch ports may not be able to handle/allow it.

If this is the case, you will need to enable *promiscuous* mode.

```
$ ip link set [NETWORK_INTERFACE] promisc on
```

MACVLAN actually has two modes. Everything up until this point has been on bridge mode. There is
also a *802.1q* mode that allows us to create sub-interfaces. This means creating something like
192.168.20.0/24.

Here is an example with actual values:

```
$ docker network create -d macvlan \
    --subnet 192.168.20.0/24 
    --gateway 192.168.20.1   
    -o parent=enp0s3.20     
    [NAME]
```

## IPVLAN

IPVLANs have two modes: *IPVLAN L2* and *IPVLAN L3*.

### IPVLAN L2

IPVLAN L2s are very similar to MACVLANs except **they solve the problem of having to set
permiscuous mode**. Containers in the network are allowed to be assigned their own IP addresses; 
however, they all share the same MAC address as the host.

Because of IPVLAN L2's similarity to MACVLANs, their network creation process is nearly identical.

### IPVLAN L3

IPVLAN L3 is all about *layer three*. This means IP addresses, routing, and routes themselves.

We do not connect containers to a network like it is a switch. **We connect them to our host as if
our host is a router**.

The reason layer three is great because within them, there is no broadcast traffic. If you have a 
complex layer two network, there is a lot of communication occurring and can cause interferance
and delays. 

A drawback, however, is containers can only communicate with other containers in their network.
By default, containers in the network can't reach the host (thus not reaching the Internet) and the 
host can't reach the containers in the network.

So why would you want to use IPVLAN L3? **To have more control over your network**.

```
$ docker network create -d ipvlan \
    --subnet [CUSTOM_SUBNET] \          # If you want to make multiple subnets, add them here as well
                                        # with additional --subnet flags
    -o parent=[NETWORK_INTERFACE] \
    -o ipvlan_mode=l3
    [NAME]
```

> **Note**
> 
> You do not need to specify a gateway because your host is the gateway.

If you do create multiple subnets in the same network, know that they can communicate with each other
because they share the same parent interface (e.g. enp0s3).

The last thing you need to do is go into your router and tell it where to go to find your subnets.
In this case, it will be your host system's IP address.

## None network

Literally nothing; no network.

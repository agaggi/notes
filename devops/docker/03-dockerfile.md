# Dockerfiles

Dockerfiles allow for the creation of new, custom Docker images. Typically, people use Dockerfiles
to extend the functionality of an already existing image to suit their needs.

## Building your first Dockerfile

Projects that use Dockerfiles will often have a file called `Dockerfile` in its top-level directory.
The filename `Dockerfile` is used by convention and is the default name Docker will look for when
building a Dockerfile. If you name your Dockerfile something else, you will have to specify the name
to Docker.

### Pulling our base image

In this example, we'll pull an image based on Alpine Linux, an extemely small and lightweight
Linux distribution:

```
$ docker pull alpine:3.19.1
```

You can see available Alpine Linux images on [Docker Hub](https://hub.docker.com/_/alpine).

> **Note**
> 
> If you are familiar with Docker images and tags, you may have noticed `alpine` or `alpine:latest`
> was not used in the `docker pull` command above. 
> 
> It is best practice to be as specific as possible with the details of the image you are pulling and
> this is determined by the tag of the image. If you use a constantly changing tag such as `latest`,
> you never really know what you're getting.

### The Dockerfile

Let's use the Docker image we just pulled to create a new image with `python3` installed. Create a
file called `Dockerfile` and put the following inside it:

```Dockerfile
FROM alpine:3.19.1

RUN apk add --no-cache python3
```

> **Note**
> 
> It is best practice to remove package cache to reduce the size of the image.
> 
> On Alpine Linux, you can add the `--no-cache` flag to `apk` or delete `/var/cache/apk/*`.

After saving your file, you can run `docker build .` to build your new image. If successful, you can
see your image by running `docker images` (or `docker image ls`):

```
$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED          SIZE
<none>       <none>    cd6e12dc4d88   47 seconds ago   49.7MB
alpine       3.19.1    05455a08881e   2 months ago     7.38MB
```

### Tagging your image

Notice the `REPOSITORY` and `TAG` are marked as `<none>`. When we created our image, we didn't give
it a name or tag. We can fix this by running:

```
$ docker build -t myimage:alpine-python .
```

```
$ docker images
REPOSITORY   TAG             IMAGE ID       CREATED          SIZE
myimage      alpine-python   cd6e12dc4d88   55 minutes ago   49.7MB
alpine       3.19.1          05455a08881e   2 months ago     7.38MB
```

### Verifying the new image has Python

You can go into your new image and see Python is in fact installed: 

```
$ docker run --rm -it myimage:alpine-python /bin/sh
/ # python --version
Python 3.11.8
```

## Common Dockerfile instructions

In Dockerfiles, there are a few types of instructions that can be executed. We've already seen `FROM`,
which specifies the base image for the new image, and `RUN`, which executes a shell command.

Reference: https://docs.docker.com/reference/dockerfile/

### `ADD`

`ADD` is similar to `COPY` but different in two main ways:

1. Files can be fetched from remote HTTPS and Git URLs
2. Local archives can be automatically exported to a specified directory in the container

Examples:

```Dockerfile
FROM scratch AS src

ARG DOTNET_VERSION=8.0.0-preview.6.23329.7
ADD --checksum=sha256:270d731bd08040c6a3228115de1f74b91cf441c584139ff8f8f6503447cebdbb \
    https://dotnetcli.azureedge.net/dotnet/Runtime/$DOTNET_VERSION/dotnet-runtime-$DOTNET_VERSION-linux-arm64.tar.gz /dotnet.tar.gz
```

```Dockerfile
FROM alpine:3.19.1

ADD my-archive.tar.zst /opt/my-archive/
```

### `ARG`

An `ARG` can be thought of as a variable that is available only when a Dockerfile is being built.

```Dockerfile
FROM alpine:3.19.1

ARG x=5
RUN echo "$x + 1 = 6"
```

### `CMD`

`CMD` sets the default command to be executed inside your container upon startup. Unlike
`ENTRYPOINT`, you can the default command that is executed. For this reason, `CMD` is more commonly
used in the wild:

```Dockerfile
FROM alpine:3.19.1
CMD ["cat", "/etc/os-release"]
```

```
$ docker run --rm -it myimage:alpine-cmd /bin/sh
/ # ls
bin    dev    etc    home   lib    media  mnt    opt    proc   root   run    sbin   srv    sys    tmp    usr    var
```

`CMD` can also be used alongside `ENTRYPOINT`; this changes its functionality. Instead of specifying
a default executable to run, `CMD` specifies the **default arguments for the executable specified
in `ENTRYPOINT`**. The default argument(s) can be changed.

```Dockerfile
FROM alpine:3.19.1

ENTRYPOINT ["uname"]
CMD ["-r"]
```

```
$ docker run --rm myimage:alpine-cmd
6.8.2
$ docker run --rm myimage:alpine-cmd -v
#1-NixOS SMP PREEMPT_DYNAMIC Tue Mar 26 22:23:34 UTC 2024
$ docker run --rm myimage:alpine-cmd -vr
6.8.2 #1-NixOS SMP PREEMPT_DYNAMIC Tue Mar 26 22:23:34 UTC 2024
```

### `COPY`

The `COPY` command supports basic copying of files inside a container. The one thing you need to
remember is that **you cannot copy files from a parent directory or higher than your Dockerfile**.

An example of a Dockerfile in a home directory that will not build:

```Dockerfile
FROM alpine:3.19.1

COPY /etc/os-release /
```

```
$ docker build -t myimage:wont-build .
 ...
 => ERROR [2/2] COPY /etc/os-release /                      0.0s
------
 > [2/2] COPY /etc/os-release /:
------
Dockerfile:3
--------------------
   1 |     FROM alpine:3.19.1
   2 |     
   3 | >>> COPY /etc/os-release /
   4 |     
--------------------
ERROR: failed to solve: failed to compute cache key: failed to calculate checksum of ref 0ae13871-a653-4a41-8984-daf3d2e79e38::bezrrgxgg9t5vxf7ovmx8z6eh: failed to walk /var/lib/docker/tmp/buildkit-mount1137140710/etc: lstat /var/lib/docker/tmp/buildkit-mount1137140710/etc: no such file or directory
```

### `ENTRYPOINT`

`ENTRYPOINT` can be thought of locking a container into doing one thing and one thing only. It
allows you to specify a default executable your container will run upon startup. **Once your
container starts, you cannot change the default executed command**.

For example:

```Dockerfile
FROM alpine:3.19.1
ENTRYPOINT ["cat", "/etc/os-release"]
```

The only thing containers based off this image is execute the `cat` command. By default, the
contents of `/etc/os-release` are outputted:

```
$ docker run --rm myimage:alpine-entrypoint
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.19.1
PRETTY_NAME="Alpine Linux v3.19"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://gitlab.alpinelinux.org/alpine/aports/-/issues"
```

### `ENV`

`ENV` sets environment variables inside an image. These variables persist even after the image is
built.

```Dockerfile
FROM alpine:3.19.1
ENV PATH=/some-dir:$PATH
```

Inside a running container based on our Dockerfile:

```
/ # echo $PATH
/some-dir:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
```

### `EXPOSE`

The `EXPOSE` instruction tells Docker that the container listens on the specified network ports at
runtime. You can choose whether a port listens on TCP or UDP (the default is TCP).

```Dockerfile
FROM alpine:3.19.1

EXPOSE 80/tcp
EXPOSE 80/udp
```

### `USER`

The `USER` instruction allows you to switch to a different user. The last user you switch to in your
Dockerfile will be the default user in all containers based off your Dockerfile.

**Your containers should not default to the root user unless you have a good reason to do so**! If
you need to create another user, you can do so using a `RUN` instruction.

### `WORKDIR`

Changing directories in a Dockerfile is not as intuitive as it may seem:

- `WORKDIR` changes the working directory for all future commands
- `RUN cd` has no impact on subsequent `RUN` commands

For example:

```Dockerfile
FROM alpine:3.19.1

WORKDIR /tmp
RUN pwd             # /tmp

RUN cd /            # no effect, resets after end of RUN line
RUN pwd             # still /tmp

WORKDIR /
RUN pwd             # /

RUN cd /tmp && pwd  # /tmp
RUN pwd             # /
```

## Dockerignore

Say in your Dockerfile you want to have a `COPY . /some-dir` instruction but there are a few files
in your project you don't want in your container(s). You can include a `.dockerignore` file in the
same directory as your Dockerfile and put the paths of the files you don't want Docker to copy into
your container(s). It works exactly the same as `.gitignore` if you are familiar with it.

## Pushing to a container registry

In this section, I'll be tagging an image and pushing it to the container registry of this GitLab
project.

### `docker login`

The first step of pushing an image is to make sure you log into the container registry you want to
push to. In my case, I would do:

```
docker login registry.gitlab.com
```

`docker login` will ask you for a username and password. You'll need to know both beforehand and
usually you'll have to create a personal access token to serve as your password.

### Re-tagging your image

My personal registry is `registry.gitlab.com/agaggi/notes`, so I will re-tag my image so that Docker
knows where to push my image:

```
$ docker tag myimage:mytag registry.gitlab.com/agaggi/notes/myimage:mytag
$ docker images
REPOSITORY                                 TAG       IMAGE ID       CREATED        SIZE
myimage                                    mytag     8fec705ba74f   24 hours ago   7.38MB
registry.gitlab.com/agaggi/notes/myimage   mytag     8fec705ba74f   24 hours ago   7.38MB
```

### Pushing the image

The last step is one simple command:

```
$ docker push registry.gitlab.com/agaggi/notes/myimage:mytag 
The push refers to repository [registry.gitlab.com/agaggi/notes/myimage]
9d1e3f04a751: Pushed 
d4fc045c9e3a: Pushed 
mytag: digest: sha256:eed5e765224ea788fe50b9ff26568e4a98335073adc06c69ea00a29ffae8e675 size: 735
```

## Multi-stage Dockerfiles

Multi-stage Dockerfiles can *drastically* reduce the size of an image. Take the following Dockerfile
for example:

```Dockerfile
FROM golang:1.21

WORKDIR /src
COPY <<EOF ./main.go
package main

import "fmt"

func main() {
  fmt.Println("hello, world")
}
EOF

RUN go build -o /bin/hello ./main.go
CMD ["/bin/hello"]
```

This is a single-stage Dockerfile that:

- Pulls the `golang:1.21` image
- Writes a `main.go` file
- Builds the `main.go` file, pulling in any needed dependencies
- Runs the produced binary

That's cool and all, but all we really care about is for our final image to run the produced binary.
If we take a look at our image, we can see all of these additional steps really add up:

```
$ docker images
REPOSITORY   TAG            IMAGE ID       CREATED         SIZE
myimage      single-stage   48dcee334e8e   9 minutes ago   842MB
```

We can cut out these extra steps by having one image perform the compilation while another image 
just runs the produced binary. We can achieve this using a multi-stage Dockerfile like so:

```Dockerfile
FROM golang:1.21 AS builder

WORKDIR /src
COPY <<EOF ./main.go
package main

import "fmt"

func main() {
  fmt.Println("hello, world")
}
EOF

RUN go build -o /bin/hello ./main.go


FROM scratch

COPY --from=builder /bin/hello /bin/hello
CMD ["/bin/hello"]
```

Let's compare the sizes of the two images:

```
$ docker images
REPOSITORY   TAG            IMAGE ID       CREATED         SIZE
myimage      multi-stage    50d81600365d   5 seconds ago   1.8MB
myimage      single-stage   48dcee334e8e   9 minutes ago   842MB
```

Wow, it's 467.78x smaller!

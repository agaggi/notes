# Installation and basic commands

On Linux, installing Docker is pretty straightforward; check your package manager. There are a few 
things you should do after installing:

1. Add your user to the `docker` group (`usermod -aG docker $USER`).
2. Refresh the user groups (`newgrp docker`).
3. Check if the Systemd service is running (`systemctl status docker`)

## Common commands

Note: You can use the actual name of an image/container *or* its hash.

| Command                             | Description                                                        |
| ----------------------------------- | ------------------------------------------------------------------ |
| `docker pull [IMAGE]`               | Pulls down a specified image from a registry                       |
| `docker push [IMAGE]:[TAG]`         | Pushes an image to a registry                                      |
| `docker run [IMAGE]`                | Creates a new container based off the specified image              |
| `docker start [CONTAINER]`          | Starts an existing container                                       |
| `docker stop [CONTAINER]`           | Stops an existing container                                        |
| `docker rm [CONTAINER]`             | Removes a container                                                |
| `docker rmi [IMAGE]`                | Removes an image                                                   |
| `docker exec [CONTAINER] [COMMAND]` | Execute a command in a running container                           |
| `docker ps`                         | Shows running containers (add the `-a` flag to see all containers) |
| `docker images`                     | Shows the installed images                                         |
| `docker logs [CONTAINER]`           | Show the logs for a container                                      |
| `docker stats`                      | Shows resource usage in real-time                                  |
| `docker rename [CONTAINER] [NAME]`  | Renames a container                                                |
| `docker inspect [CONTAINER]`        | Outputs detailed container information in JSON format              |
| `docker attach [CONTAINER]`         | Brings a detached container to the front                           |
| `docker logs [CONTAINER]`           | View the logs of a container                                       |

## Example commands

```
$ docker run --name [NAME] -it -d --rm [IMAGE] [COMMAND]
```

This is an example of a `docker run` command that uses many of the most common flags:

- `--name` allows you to personalize the name of your container. If you don't set this, a random 
  name will be assigned to the container instead.
- `-i` stands for interactive and `-t` allocated a pseudo-tty. `-it` together create a virtual
  shell you can remote into.
- `-d` represents 'detached'. Your container will run in the background instead of taking up your
  terminal. You can bring it back to the front using `docker attach`.
- `--rm` will delete the container after it is done running or if it is stopped.

You do not have to have a command at the end of `docker run` commands. It acts just like of you ran
`docker exec`. A common command is `/bin/bash` if you want to keep a container alive longer than
it's typically supposed to be and you can remote into it.

```
$ docker run -e [VARIABLE]=[VALUE] -p [HOST PORT]:[CONTAINER PORT] -v /path/to/host-dir/:/path/to/container-dir
```

This command sets an environmental variable (think of `$PATH`, etc.), maps a port from the docker 
container to the host, and uses volumes.

You can also create a volume using `docker volume create [NAME]` and use the name instead of a host
system path. Additionally, you can use `docker volume ls` to see volumes on your system.

**Databases and other programs may need a default password. You can use environmental variables to
set them.**

# Python functions

A function is a section of code that is only executed only when called. Functions can take in values
called *parameters* and also return values, but this is not required.

There are a number of functions that are built-in to Python. You can find them here:
https://docs.python.org/3/library/functions.html.

## Function basics

Below is an example of a typical function in Python that takes in values as parameters and returns a
new value:

```python
def add(a, b):
    return a + b
```

While Python is not a strongly typed language, you will often see type hints provided to let
programmers and language server protocols (LSPs) know what type of variables your function is taking
in and returning:

```python
def add(a: int, b: int) -> int:
    return a + b
```

Below is an example of a function that returns nothing:

```python
def print_message() -> None:
    print("Hello world!")
```

### Calling functions

Using the following functions as an example...

```python
def add(a: int, b: int) -> int:
    return a + b

def print_message() -> None:
    print("Hello world!")

def greeting(firstname: str, lastname: str) -> None:
    print(f'Hello {firstname} {lastname}')
```

If we wanted to call them, we can do so like this:

```python
result = add(9, 10)
print_message()
greeting(lastname='Gaggiotti', firstname='Alex')
```

> **Note**
> 
> We assign the `add` function to a variable because it returns a value. Otherwise, we can't use the
> returned value.

## `lambda` functions

Say we had the following dictionary and we wanted to sort by age:

```
>>> people = [{"name": "Alex", "age": 24}, {"name": "John", "age": 30}, {"name": "Jane", "age": 16}]
```

We could do so by creating a function that returns the key we want to sort by, which can be really
annoying depending on the data type you are working with, or you can use a `lambda` function:

```
>>> people.sort(key=lambda person: person['age'])
>>> people
[{'name': 'Jane', 'age': 16}, {'name': 'Alex', 'age': 24}, {'name': 'John', 'age': 30}]
```

A `lambda` is an inline, anonymous function that can take any number of arguments; however, it can
only have one expression. Sorting objects by a certain element is by far its most common use-case.

You can create simple inline functions too since functions are *first-class objects*, meaning they
can be assigned to variables just like any ordinary value.

```
>>> multiply = lambda a, b: a * b
>>> multiply(5, 5)
25
```

## `*args` and `**kwargs`

`*args` and `**kwargs` allow you to pass a variable amount of arguments to a function.

- `*args` is used to pass a variable number of positional arguments to a function. The arguments are
  collected in a `tuple`.
- `**kwargs` allows you to pass in a variable number of keyword arguments that are collected in a
  dictionary

Here's an example:

```
>>> def my_func(*args, **kwargs):
...     print("Positional arguments (*args):", args)
...     print("Keyword arguments (**kwargs):", kwargs)
... 
>>> my_func(1, 2, 3, name="Alice", age=30)
Positional arguments (*args): (1, 2, 3)
Keyword arguments (**kwargs): {'name': 'Alice', 'age': 30}
```

## Decorator functions

Decorators are a powerful tool that allows programmers to modify the behavior of a function without
having to change the function itself.

### Concepts to understand before using decorators

1. A function is a first-class object in Python, meaning it can be assigned to variables:
  
   ```python
   def my_function():
       print('I am a function.')

   description = my_function
   description()
   ```

2. Functions can be nested in other functions:

   ```python
   def outer_function():
       def inner_function():
           print('I came from the inner function.')

       inner_function()

   outer_function()                 # Output: 'I came from the inner function.'
   ```

   > **Note**

   > If you try to execute `inner_function()` outside of `outer_function()`, you will get a
   > `NameError` exception.

3. A nested function can actually be returned:

   ```python
   def outer_function():
       task = 'Read Python book chapter 3.'
    
       def inner_function():
           print(task)
   
       return inner_function

   homework = outer_function()      # Output: 'Read Python book chapter 3'
   ```

4. Functions can be passed to other functions as arguments:

   ```python
   def foo(function):
       function()
       print('foo')

   def bar():
       print('bar')

   foo(bar)                         # Output: 'bar' and then 'foo'
   ```

### Creating a decorator

To create a decorator, you will need to create an outer function that takes a function as an
argument. Then, inside the outer function, there needs to be an inner function:

```python
def decorator_function(function):
    def wrapper_function():
        # Do stuff before the call
        function()
        # Do stuff after the call 

    return wrapper_function
```

So we've created a decorator function above. Now to use it with any other function, we'll need to
use special syntax:

```python
@decorator_function
def my_function():
    pass
```

With this syntax, it is just like passing our `my_function()` function directly into our decorator
function, `decorator_function()`.

Below is a real use-case for decorator functions:

```python
from collections.abc import Callable
from datetime import datetime

def log_timestamp(fn: Callable) -> Callable:
    '''Log the timestamp a function was run.'''

    def wrapper() -> None:
        fn()
        print(f'Function: {fn.__name__}\nRun on: {datetime.today().strftime("%Y-%m-%d %H:%M:%S")}')
        print(f'{"-" * 35}')

    return wrapper

@log_timestamp
def daily_backup() -> None:
    # Do other stuff
    print("Daily backup finished.")

daily_backup()
```

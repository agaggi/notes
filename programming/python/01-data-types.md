# Python data types

Python has the following built-in types:

| Group                 | Types                                 |
| --------------------- | ------------------------------------- |
| Boolean Types         | `bool`                                |
| Numeric Types         | `int`, `float`, `complex`             |
| Text Sequence Types   | `str`                                 |
| Binary Sequence Types | `bytes`, `bytearray`, `memoryview`    |
| Sequence Types        | `list`, `tuple`, `range`              |
| Set Types             | `set`, `frozenset`                    |
| Mapping Types         | `dict`                                |

**References**: 

- https://docs.python.org/3/library/stdtypes.html

## The boolean type

Booleans represent truth values. A type `bool` variable can only be `True` or `False`.

There are a few boolean operators:

| Operation | Notes                                                                             |
| --------- | --------------------------------------------------------------------------------- |
| `x or y`  | Short-circuit operator: only evaluates second argument if the first is `False`    | 
| `x and y` | Short-circuit operator: only evaluates second argument if the first is `True`     |
| `not x`   | N/A                                                                               |

There are also bitwise operators. When applied to two booleans, a boolean equivalent is returned:

| Operator  | Equivalent    |
| --------- | ------------- |
| `x & y`   | "and"         |
| `x \| y`  | "or"          |
| `x ^ y`   | "xor"         |

## Numeric types

There are three numeric types: *integers*, *floating point numbers*, and *complex numbers*.

Additionally, Booleans are a subtype of integers.

- `False` => 0
- `True` => anything else

Example:

```python
if 0: print("This will not execute!")
if 20: print("This will execute!")
```

**References**: 

- https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex
- https://docs.python.org/3/library/math.html

### `int`

Python has no limit to how big an integer can be. The only constraint is the amount of memory your
computer has.

```
>>> 123123123123123123123123123123123123123123123123 + 1
123123123123123123123123123123123123123123123124
```

For an *extremely* large integer, you can get a `ValueError` when trying to print it:

```
>>> 123 ** 10000
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: Exceeds the limit (4300 digits) for integer string conversion; use sys.set_int_max_str_digits() to increase the limit
```

You can also represent base integers other than `10`:

| Prefix        | Representation    | Base  |
| ------------- | ----------------- | ----- |
| `0b` or `0B`  | Binary            | 2     |
| `0o` or `0O`  | Octal             | 8     |
| `0x` or `0X`  | Hexadecimal       | 16    |

The `int()` constructor can be used to convert between bases:

```
>>> int("0b10", base=2)
2
>>> int("10", base=8)
8
>>> int("10", base=16)
16
```

### `float`

Floating point numbers are usually the equvalent to the `double` type in the C programming language.
Like in C, the precision of floating point numbers depends on your machine. You can check the degree
of precision you can achieve with `sys.float_info`.

### `complex`

Complex numbers have a real and imaginary part. The real and imaginary parts of a complex number can
be extracted by using the `.real` and `.imag` attributes respectively.

Example:

```
>>> complex(4, 5)
(4+5j)
>>> complex(3.5, 2.75).real
3.5
>>> complex('3.5+2.75j').imag
2.75
```

## The text sequence type

Textual data in Python is handled by `str` objects, or strings. Strings are immutable sequences of
Unicode. String literals can be written in a few ways:

```python
single_quoted = 'Allows embedded "double" quotes'
double_quoted = "Allows embedded 'single' quotes"

triple_quoted = '''The
leading
  whitespace
    really
matters
'''
```

You can access individual elements of a string similarly to what you can do with a list (but
remember you can't change a string because it's immutable!):

```
>>> name = 'Alex'
>>> 'Al' in name        # checks if the sequence 'Al' appears in 'Alex'
True
>>> name[-1]            # reverse-indexing
'x'
>>> name[0:3]           # index-slicing - retrieve values from indexes [0, 3)
'Ale'
>>> name[1:]            # index-slicing - retrieve values [1, END]
'lex'
```

Python also has *formatted strings* which is a convenient way to embed variables in a string. If you
want to use a formatted string, you will need to have a leading `f` in front of the string literal:

```
>>> name = 'Alex'
>>> f"Hello, my name is {name}"
'Hello, my name is Alex'
```

You can also join strings together by putting them in a `list` and using the `join` function:

```
>>> ' '.join(["Hello", "world!"])
'Hello world!'
```

**References**:

- https://docs.python.org/3/library/stdtypes.html#string-methods
- https://docs.python.org/3/library/stdtypes.html#typesseq-common

## Binary sequence types

The core built-in types for manipulating binary data are `bytes` and `bytearray`. These types are
supported by `memoryview` which uses the 
[buffer protocol](https://docs.python.org/3/c-api/buffer.html#bufferobjects) to access the memory of
binary objects without needing to make a copy of them.

Both `bytes` and `bytearray` support the 
[common sequence operations](https://docs.python.org/3/library/stdtypes.html#typesseq-common) like 
strings. 

### `bytes`

`bytes` objects are immutable sequences of single bytes. Many major binary protocols are based on
ASCII text encoding so there are several `bytes` methods that work only with ASCII-compatible data.

The syntax for `bytes` is largely the same as for `str` except **a leading `b` prefix is added**:

```python
single_quoted = b'Allows embedded "double" quotes'
double_quoted = b"Allows embedded 'single' quotes"

triple_quoted = b'''The
leading
  whitespace
    really
matters
'''
```

Only ASCII characters are allowed in `bytes` literals. Any binary values
[over 127](https://www.asciitable.com/) must be entered using the appropriate escape sequence.

While `bytes` literals are based on ASCII text, `bytes` objects actually behave like an immutable
sequence of integers with each value restricted to the bounds of `0 <= x < 256` to avoid throwing
a `ValueError`.

Since two hexadecimal digits correspond precisely to a single byte, hexadecimal numbers are a
commonly used format for describing binary data.

Example:

```
>>> a = b"15"
>>> a.hex()
'3135'
>>> bytes.fromhex(a.hex())
b'15'
```

### `bytearray`

`bytearray` objects are the mutable counterpart to `bytes` objects.

## Sequence types

There are three basic sequence types: lists, tuples, and range objects. There are additional
sequence types for processing binary and textual data, `bytes` (or `bytearray`) and `str`
respectively.

Sequence types have
[common sequence operations](https://docs.python.org/3/library/stdtypes.html#typesseq-common) that
can be performed on them.

### `list`

Lists are mutable sequences that are typically used to store collections of items of the same type.
Unlike in other programming languages, however, having a collection where all elements are the same
type is not a requirement. This is because Python is not a staticly-typed language but rather a
dynamicaly-typed one.

Example:

```python
numbers = [1, 2, 3, 4, 5]           # Valid
random = [True, "abcd", 25, None]   # Valid, but usually not recommended
```

Along with the common sequence operations linked above, lists have their own `sort` function:

```
>>> a = [23, 1, 45, 33, 55]
>>> a.sort()
>>> a
[1, 23, 33, 45, 55]
```

```
>>> names = ["Johnathan", "Bob", "Jane"]
>>> names.sort(reverse=True)
>>> names
['Johnathan', 'Jane', 'Bob']
```

> **Note**
> 
> An important distinction between the built-in `sorted()` function and `list.sort()` is that 
> `list.sort()` sorts a list **in-place** whereas `sorted()` creates an entirely new list (see
> https://docs.python.org/3/howto/sorting.html).

### `tuple`

Tuples are like lists except that they are immutable. If you know your sequence will not change after
being defined, it's recommended to use a `tuple` over a `list`.

```
>>> numbers = (1, 2, 3, 4, 5)
>>> numbers[0] = 6
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
```

### `range`

The `range` type represents an immutable sequence of numbers. It's most common use-case is with
looping a specific number of times in a `for`-loop.

```
>>> for i in range(6):
...     print(i)
... 
0
1
2
3
4
5
```

The `range()` constructor can be called in two different ways:

1. `range(stop)`
2. `range(start, stop[, step])`

```
>>> list(range(10))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> list(range(1, 11))
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
>>> list(range(0, 30, 5))
[0, 5, 10, 15, 20, 25]
>>> list(range(0, 10, 3))
[0, 3, 6, 9]
>>> list(range(0, -10, -1))
[0, -1, -2, -3, -4, -5, -6, -7, -8, -9]
>>> list(range(0))
[]
```

## Set types

A set object is a collection of unordered, hashable objects. Common use-cases for sets include
membership testing, removing duplicates from a sequence, and computing mathematical operations such
as intersection, union, difference, and symmetric difference.

There are currently two built-in set types: `set` and `frozenset`. The `set` type is mutable, so
functions like `add()` and `remove()` can be used to change its contents. Because `set` objects are
mutable, they have no hash value and **cannot** be used as a dictionary key or as an element of 
another set. 

`frozensets`, on the other hand, can do all of these things because they *are* immutable.

> **Note**
> 
> Searching through sets can be significantly faster than searching through lists.
> 
> - Set search time: O(1)
> - List search time: O(n)
> 
> Example:
> 
> ```
> >>> if 'Alex' in ['Alex', 'John', 'Jane']:
> ...     print('Alex is in there...')
> ... 
> Alex is in there...
> ```

**References**:

- https://docs.python.org/3/library/stdtypes.html#set
- https://www.youtube.com/watch?v=r3R3h5ly_8g

## Mapping types

A mapping object maps hashable values to arbitrary objects. Mappings are mutable objects and there
is currently only one standard mapping type: the dictionary type, `dict`.

Values that are not hashable, such as lists, dictionaries, or other mutable types, may not be used
as *keys*.

Dictionaries can be created in several ways:

- By using a comma-separated list of `key: value` pairs within braces
  
  ```
  >>> {"a": 1, "b": 2, "c": 3}
  {'a': 1, 'b': 2, 'c': 3}
  ```

- By using a `dict` comprehension

  ```
  >>> {x: x ** 2 for x in range(10)}
  {0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81}
  ```

- By using the `dict` type constructor

  ```
  >>> dict(one=1, two=2, three=3)
  {'one': 1, 'two': 2, 'three': 3}
  ```

Below shows many more ways you can define dictionaries (note: they all evaluate to `{"one": 1,
"two": 2, "three": 3}`):

```
>>> a = dict(one=1, two=2, three=3)
>>> b = {'one': 1, 'two': 2, 'three': 3}
>>> c = dict(zip(['one', 'two', 'three'], [1, 2, 3]))
>>> d = dict([('two', 2), ('one', 1), ('three', 3)])
>>> e = dict({'three': 3, 'one': 1, 'two': 2})
>>> f = dict({'one': 1, 'three': 3}, two=2)
>>> a == b == c == d == e == f
True
```

**References**:

- https://docs.python.org/3/library/stdtypes.html#dict

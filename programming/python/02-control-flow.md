# Control flow

## `if` statements

`if` statements allow you to control what code is executed based on conditions. Below's a typical
example of one:

```python
age = int(input("Enter your age: "))

if age < 18:
    print("You are a child")
elif age >= 18:
    print("You are an adult")
else:
    print("Invalid age entered")
```

Python also has a *ternary operator*, which is a short-handed `if` statement:

```python
value = 1 if condition is True else 0
```

> **Note**
> 
> The `is` and `is not` operators check if two variables point to the same object, not if two
> variables have the same value. 
> 
> Example:
> 
> ```
> >>> a = True
> >>> a is True
> True
> >>> a = [1, 2, 3]
> >>> a is [1, 2, 3]
> False
> ```

## Loops

There are two kinds of loops in Python: `for` and `while`.

### `for` loops

`for` loops iterate over a given sequence.

The following is a typical `for` loop in Python:

```
>>> names = ["Alex", "John", "Jane"]
>>> for name in names:
...     print(name)
... 
Alex
John
Jane
```

If you have experience in another programming language, you may be tempted to iterate over the
*indexes* of a collection rather than the *elements* themselves:

```
>>> names = ["Alex", "John", "Jane"]
>>> for i in range(len(names)):
...     print(names[i])
... 
Alex
John
Jane
```

While technically correct, it's a bit harder to read and shows others you are new to the language.
If you really need to keep track of indexes as you loop, use the `enumerate()` function:

```
>>> names = ["Alex", "John", "Jane"]
>>> for i, name in enumerate(names):
...     if i % 2 != 0:
...         names[i] = name.upper()
...     print(name)
... 
Alex
JOHN
Jane
```

And, if you want to iterate over two collections at the same time, you can use the `zip()` function:

```
>>> names = ["Alex", "John", "Jane"]
>>> ages = [24, 36, 32]
>>> for i, (name, age) in enumerate(zip(names, ages)):
...     if i % 2 != 0:
...         names[i] = name.upper()
...     print(f'{name} is {age} years old')
... 
Alex is 24 years old
John is 36 years old
Jane is 32 years old
```

One last and lesser known feature of `for` loops is the `for-else` feature. If an `else` clause is
placed after a `for` loop, it will be executed **as long as the `for` loop did not `break` early**:

```
>>> for i in range(5):
...     if i == 1:
...             break
... else:
...     print("We didn't exit early")
... 
>>> 
```

### `while` loops

`while` loops keep running until a condition is met:

```python
height_in = 0

while height_in <= 0:
    height_in = int(input("Enter your height in inches: "))

print(f"You are {height_in * 2.54:.2f} centimeters tall")
```

Also, like with `for` loops, you can have a `while-else` clause that will execute as long as your
loop finishes and doesn't break. 

Both `for` and `while` loops have a keyword that can be used inside them much like `break`:
`continue`. The `continue` keyword skips an iteration when reached rather than breaking out of the
loop all together:

```python
height_in = 0

while height_in <= 0:
    try:
        height_in = int(input("Enter your height in inches: "))
    except ValueError:
        continue

print(f"You are {height_in * 2.54:.2f} centimeters tall")
```

## `match` statements

A `match` statement takes an expression and compares its value to successive patterns. 

If you are familiar with C, Java, etc., you may compare `match` to `switch` statements; however,
it's more like pattern matching in languages such as Rust and Haskell. **Only the first pattern that
matches gets executed**.

Example:

```python
def http_error(status):
    match status:
        case 400:
            return "Bad request"
        case 404:
            return "Not found"
        case 418:
            return "I'm a teapot"
        case _:
            return "Something's wrong with the internet"
```

You can combine several literals into a single pattern using the `|` ("or") operator:

```python
case 401 | 403 | 404:
    return "Not allowed"
```

**References**:

- https://docs.python.org/3/tutorial/controlflow.html#match-statements

## Exception handling

Exeption handling is used to catch errors that we expect. For example, if a user inputs an invalid
value, we can account for it. See https://docs.python.org/3/library/exceptions.html for all of the
built-in exceptions.

`try-except` statements can have the following structure:

```python
try:
    # Code that might throw an exception
except Exception:
    # Code that handles the possible exception, correctly
else:
    # (OPTIONAL) Code that runs if there are no exceptions thrown
finally:
    # (OPTIONAL) Code that always runs after 
```

Example 1:

```python
height_in = 0

while height_in <= 0:
    try:
        height_in = int(input("Enter your height in inches: "))
    except ValueError:
        continue

print(f"You are {height_in * 2.54:.2f} centimeters tall")
```

Example 2:

```python
try:
    file = open("myfile.txt")
except FileNotFoundError as e:
    print(e)
else:
    print("file opened")
    file.close()
finally:
    print("End of program")
```

### Raising your own exceptions

If you want to raise your own exceptions, you can use the `raise` keyword followed by the desired
exception:

```python
try:
    age = int(input("Enter your age: "))

    if age <= 0:
        raise Exception
except ValueError:
    print("Age must be an integer")
except Exception:
    print("Age must be greater than 0")
```

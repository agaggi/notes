# Classes

Classes provide a means of bundling related data and functionality together. Creating a new `class`
can be thought of creating a new type. Classes can have *attributes* and *methods* attached to them.

- Attributes = Class variables
- Methods = Class functions

Example:

```python
class Entity:
    # Class constructor - called everytime a new class instance is created
    def __init__(self, x: int, y: int) -> None:
        # Defining the class attributes
        self.x = x
        self.y = y
    
    # Defining a class method 
    def print_position(self) -> None:
        print((x, y))

# Creating a new instance of the Entity class
e = Entity(5, 5)
e.print_position()
```

## Inheritence

Python supports *inheritence*, meaning it can... inherit methods and attributes from other classes.

Take the following classes for example:

```python
class Mammal:
    def eat(self):
        print("Eat")

    def sleep(self):
        print("Sleep")

    def poop(self):
        print("Poop")

# Dog inherits from the superclass Mammal
class Dog(Mammal):
    pass

d = Dog()
d.poop()
```

## Dunder methods

Dunder methods are special functions that exist in every class. If you are familiar with C++ and the
concept of *operator overloading*, you can use dunder methods to change how Python handles instances
of your class in certain situations.

Example:

```python
class Person:
    def __init__(self, name: str) -> None:
        self.name = name

    def __str__(self) -> str:
        return f"Hi my name is {self.name}"

p = Person("Alex")
print(p)                # Usually, you shouldn't be able to do this
```

**References**:

- https://docs.python.org/3/reference/datamodel.html#special-method-names

## Class decorators

If you really want, you can use classes instead of functions as decorators:

```python
class Decorator:
    def __init__(self, function):
        self.function = function

    def __call__(self, *args, **kwargs):
        print("yo")
        return self.function(*args, **kwargs)


@Decorator
def output(name):
    print(f'Hello {name}!')

output('Alex')
```

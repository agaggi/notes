# Generators

Generators allow you to declare a function that acts like an iterator. To know the purpose of
generators, consider the following example:

```python
def first_n(n: int) -> list[int]:
    num, nums = 0, []

    while num < n:
        nums.append(num)
        num += 1

    return nums

sum_of_first_n = sum(first_n(1000000))
```

The code is simple but the full list of numbers is actually being allocated in memory. Depending on
how constrained you are, you may not be able to afford this.

We can create a generator function that gives us the values we need **as we need them** rather than
all at once. This significantly reduces the burden on memory. We also don't have to wait for all of
the values to be generated to start using them.

```python
from typing import Generator

def firstn(n: int) -> Generator[int, None, None]:
    num = 0

    while num < n:
        yield num
        num += 1

sum_of_first_n = sum(firstn(1000000))
```

## Comprehensions

Much like how we can do list comprehensions, we can do the same for generators with little change
to syntax:

```python
# List comprehension
doubles = [2 * n for n in range(50)]

# Generator comprehension
doubles = list(2 * n for n in range(50))
```

Here, in the second comprehension, a generator expression is being passed to the `list` constructor.
With generator expressions, we don't have to write a whole function if we don't need to get a list
out of it.

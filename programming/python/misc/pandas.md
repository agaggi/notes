# Pandas

Pandas is a Python package that provides fast, flexible, and expressive data structures that are
designed to work with *relational* or *labeled* data easily such as:

- Tabular data with heterogeneously-typed columns (e.g., SQL tables, Excel spreadsheets)
- Ordered and unordered time series data
- Any other form of observational or statistical data sets; the data does not need to be labeled

Pandas has a number of add-ons you can choose from when installing. It's better to look for yourself
[here](https://pandas.pydata.org/docs/getting_started/install.html).

Pandas is an external Python package. It can be installed through conda, pip, or a package manager
on Linux/MacOS. You will need to import the library if you want to use it:

```python
import pandas as pd
```

**Reference**: https://pandas.pydata.org/docs/user_guide/10min.html

## Data structures

Pandas provides two data structures:

1. `Series`: a one-dimensional labeled array holding data of any type
2. `DataFrame`: a two-dimensional data structure that holds data like a two-dimension array or a 
    table with rows and columns

The best way to think about Pandas data structures are as flexible containers for lower dimensional
data. For example, a `DataFrame` is a container for `Series` and `Series` are containers for
scalars. **We want to be able to insert/remove objects from these containers in a dictionary-like
fashion**.

### Object creation

A `Series` object is created by passing in a list of values:

```
>>> pd.Series([1, 2, 3, np.nan, 5, 6])
0    1.0
1    2.0
2    3.0
3    NaN
4    5.0
5    6.0
dtype: float64
```

A `DataFrame` is a bunch of `Series` grouped together:

```
>>> dates = pd.date_range("20240101", periods=6)
>>> dates
DatetimeIndex(['2024-01-01', '2024-01-02', '2024-01-03', '2024-01-04',
               '2024-01-05', '2024-01-06'],
              dtype='datetime64[ns]', freq='D')
>>> df = pd.DataFrame(np.random.randn(6, 4), index=dates, columns=list("ABCD"))
>>> df
                   A         B         C         D
2024-01-01  0.210789 -1.071055 -0.342249 -0.273835
2024-01-02 -1.255592  1.534915  1.507631  1.858962
2024-01-03 -0.627896 -1.097062 -0.553284  1.024856
2024-01-04 -0.043681  0.155684  0.111159 -1.138637
2024-01-05 -0.159455  0.268021  1.032060  1.357560
2024-01-06  0.141559  1.115468 -0.065667  0.214654
```

Here's an example using a dictionary:

```
>>> df2 = pd.DataFrame(
... 
...     {
... 
...         "A": 1.0,
... 
...         "B": pd.Timestamp("20130102"),
... 
...         "C": pd.Series(1, index=list(range(4)), dtype="float32"),
... 
...         "D": np.array([3] * 4, dtype="int32"),
... 
...         "E": pd.Categorical(["test", "train", "test", "train"]),
... 
...         "F": "foo",
... 
...     }
... 
... )
>>> df2
     A          B    C  D      E    F
0  1.0 2013-01-02  1.0  3   test  foo
1  1.0 2013-01-02  1.0  3  train  foo
2  1.0 2013-01-02  1.0  3   test  foo
3  1.0 2013-01-02  1.0  3  train  foo
```

Notice how different columns are different types:

```
>>> df2.dtypes
A          float64
B    datetime64[s]
C          float32
D            int32
E         category
F           object
dtype: object
```

## Viewing data

You can use the `.head()` and `.tail()` functions to view the first or last *n* values:

```
>>> df.head()
                   A         B         C         D
2024-01-01  0.210789 -1.071055 -0.342249 -0.273835
2024-01-02 -1.255592  1.534915  1.507631  1.858962
2024-01-03 -0.627896 -1.097062 -0.553284  1.024856
2024-01-04 -0.043681  0.155684  0.111159 -1.138637
2024-01-05 -0.159455  0.268021  1.032060  1.357560
>>> df.tail(2)
                   A         B         C         D
2024-01-05 -0.159455  0.268021  1.032060  1.357560
2024-01-06  0.141559  1.115468 -0.065667  0.214654
```

The `.index` and `.columns` attributes show the indexes and columns of the `DataFrame` object:

```
>>> df.index
DatetimeIndex(['2024-01-01', '2024-01-02', '2024-01-03', '2024-01-04',
               '2024-01-05', '2024-01-06'],
              dtype='datetime64[ns]', freq='D')
>>> df.columns
Index(['A', 'B', 'C', 'D'], dtype='object')
```

`DataFrame` objects can be converted to NumPy arrays:

```
>>> df.to_numpy()
array([[ 0.21078936, -1.07105503, -0.34224949, -0.27383547],
       [-1.2555921 ,  1.53491524,  1.5076315 ,  1.85896224],
       [-0.62789647, -1.09706182, -0.55328428,  1.02485604],
       [-0.04368095,  0.15568436,  0.11115867, -1.13863746],
       [-0.15945498,  0.26802056,  1.03206006,  1.35756043],
       [ 0.14155875,  1.11546834, -0.06566686,  0.21465434]])
```

> **Note:**
> 
> NumPy arrays are meant to have one `dtype`. Pandas will try to find a `dtype` that accommodates 
> all entries. If one isn't found, the generic `Object` type will be used.

`.describe()` shows a quick statistical summary of your data:

```
>>> df.describe()
              A         B         C         D
count  6.000000  6.000000  6.000000  6.000000
mean  -0.289046  0.150995  0.281608  0.507260
std    0.558800  1.087218  0.812676  1.115592
min   -1.255592 -1.097062 -0.553284 -1.138637
25%   -0.510786 -0.764370 -0.273104 -0.151713
50%   -0.101568  0.211852  0.022746  0.619755
75%    0.095249  0.903606  0.801835  1.274384
max    0.210789  1.534915  1.507631  1.858962
```

A `DataFrame` can be transposed:

```
>>> df.T
   2024-01-01  2024-01-02  2024-01-03  2024-01-04  2024-01-05  2024-01-06
A    0.210789   -1.255592   -0.627896   -0.043681   -0.159455    0.141559
B   -1.071055    1.534915   -1.097062    0.155684    0.268021    1.115468
C   -0.342249    1.507631   -0.553284    0.111159    1.032060   -0.065667
D   -0.273835    1.858962    1.024856   -1.138637    1.357560    0.214654
```

Axes can be sorted:

```
>>> df.sort_index(axis=0, ascending=False)
                   A         B         C         D
2024-01-06  0.141559  1.115468 -0.065667  0.214654
2024-01-05 -0.159455  0.268021  1.032060  1.357560
2024-01-04 -0.043681  0.155684  0.111159 -1.138637
2024-01-03 -0.627896 -1.097062 -0.553284  1.024856
2024-01-02 -1.255592  1.534915  1.507631  1.858962
2024-01-01  0.210789 -1.071055 -0.342249 -0.273835
>>> df.sort_index(axis=1, ascending=False)
                   D         C         B         A
2024-01-01 -0.273835 -0.342249 -1.071055  0.210789
2024-01-02  1.858962  1.507631  1.534915 -1.255592
2024-01-03  1.024856 -0.553284 -1.097062 -0.627896
2024-01-04 -1.138637  0.111159  0.155684 -0.043681
2024-01-05  1.357560  1.032060  0.268021 -0.159455
2024-01-06  0.214654 -0.065667  1.115468  0.141559
```

You can also sort a `DataFrame` base on a column:

```
>>> df.sort_values(by="B")
                   A         B         C         D
2024-01-03 -0.627896 -1.097062 -0.553284  1.024856
2024-01-01  0.210789 -1.071055 -0.342249 -0.273835
2024-01-04 -0.043681  0.155684  0.111159 -1.138637
2024-01-05 -0.159455  0.268021  1.032060  1.357560
2024-01-06  0.141559  1.115468 -0.065667  0.214654
2024-01-02 -1.255592  1.534915  1.507631  1.858962
```

## Selection

Traditional Python and Numpy methods can be used for selecting and setting values; however, there
are a number of optimized Pandas access methods recommended to be used instead:

- `DataFrame.at()`
- `DataFrame.iat()`
- `DataFrame.loc()`
- `DataFrame.iloc()`

### Getitem (`[]`)

Passing a single label selects the column and yields a Series consisting of its values:

```
>>> df["A"]
2024-01-01   -0.153966
2024-01-02   -0.705585
2024-01-03   -0.252943
2024-01-04    1.512004
2024-01-05   -0.025248
2024-01-06   -0.869568
Freq: D, Name: A, dtype: float64
```

Passing a slice selects matching rows:

```
>>> df[0:3]
                   A         B         C         D
2024-01-01 -0.153966  0.255813 -1.762178  0.182129
2024-01-02 -0.705585  0.045678 -0.427188 -0.916598
2024-01-03 -0.252943 -1.099356  0.773502 -0.294143
```

### Selection by label

Selecting a row matching a label:

```
>>> df.loc[dates[0]]
A   -0.153966
B    0.255813
C   -1.762178
D    0.182129
Name: 2024-01-01 00:00:00, dtype: float64
```

Selecting all rows with specified column labels:

```
>>> df.loc[:, ['A', 'B']]
                   A         B
2024-01-01 -0.153966  0.255813
2024-01-02 -0.705585  0.045678
2024-01-03 -0.252943 -1.099356
2024-01-04  1.512004 -0.329588
2024-01-05 -0.025248  0.814907
2024-01-06 -0.869568  1.597306
```

```
>>> df.loc['20240102':'20240104', ['A', 'B']]
                   A         B
2024-01-02 -0.705585  0.045678
2024-01-03 -0.252943 -1.099356
2024-01-04  1.512004 -0.329588
```

Selecting a single row and column returns a value:

```
>>> df.at[dates[0], 'A']
-0.15396622029433388
```

### Selection by position

To select values at a certain position in each row:

```
>>> df.iloc[3]
A    1.512004
B   -0.329588
C   -2.308834
D    0.007813
Name: 2024-01-04 00:00:00, dtype: float64
```

Integer slices act similar to Python/Numpy:

```
>>> df.iloc[1:4, 0:2]
                   A         B
2024-01-02 -0.705585  0.045678
2024-01-03 -0.252943 -1.099356
2024-01-04  1.512004 -0.329588
```

Lists can be used as filters too:

```
>>> df.iloc[[0, 3, 4], [1, 3]]
                   B         D
2024-01-01  0.255813  0.182129
2024-01-04 -0.329588  0.007813
2024-01-05  0.814907  0.816508
```

For slicing rows explicitly:

```
>>> df.iloc[2:3, :]
                   A         B         C         D
2024-01-03 -0.252943 -1.099356  0.773502 -0.294143
```

For slicing columns explicitly:

```
>>> df.iloc[:, 2:]
                   C         D
2024-01-01 -1.762178  0.182129
2024-01-02 -0.427188 -0.916598
2024-01-03  0.773502 -0.294143
2024-01-04 -2.308834  0.007813
2024-01-05  0.147681  0.816508
2024-01-06  0.115712 -2.568746
```

For getting a value explicitly:

```
>>> df.iat[1, 1]
0.04567797814860178
```

### Boolean indexing

Select rows where `A` column values are greater than 0:

```
>>> df[df['A'] > 0]
                   A         B         C         D
2024-01-04  1.512004 -0.329588 -2.308834  0.007813
```

Select values from a `DataFrame` where a condition is met:

```
>>> df[df > 0]
                   A         B         C         D
2024-01-01       NaN  0.255813       NaN  0.182129
2024-01-02       NaN  0.045678       NaN       NaN
2024-01-03       NaN       NaN  0.773502       NaN
2024-01-04  1.512004       NaN       NaN  0.007813
2024-01-05       NaN  0.814907  0.147681  0.816508
2024-01-06       NaN  1.597306  0.115712       NaN
```

The `isin` method can be used for filtering:

```
>>> df2 = df.copy()
>>> df2["E"] = ["one", "one", "two", "three", "four", "three"]
>>> df2
                   A         B         C         D      E
2024-01-01 -0.153966  0.255813 -1.762178  0.182129    one
2024-01-02 -0.705585  0.045678 -0.427188 -0.916598    one
2024-01-03 -0.252943 -1.099356  0.773502 -0.294143    two
2024-01-04  1.512004 -0.329588 -2.308834  0.007813  three
2024-01-05 -0.025248  0.814907  0.147681  0.816508   four
2024-01-06 -0.869568  1.597306  0.115712 -2.568746  three
>>> df2[df2["E"].isin(["one", "four"])]
                   A         B         C         D     E
2024-01-01 -0.153966  0.255813 -1.762178  0.182129   one
2024-01-02 -0.705585  0.045678 -0.427188 -0.916598   one
2024-01-05 -0.025248  0.814907  0.147681  0.816508  four
```

### Setting

Setting a new column automatically aligns data by indexes:

```
>>> s1 = pd.Series([1, 2, 3, 4, 5, 6], index=pd.date_range("20240101", periods=6))
>>> s1
2013-01-02    1
2013-01-03    2
2013-01-04    3
2013-01-05    4
2013-01-06    5
2013-01-07    6
Freq: D, dtype: int64
>>> df["F"] = s1
>>> df
                   A         B         C         D   F
2024-01-01 -0.153966  0.255813 -1.762178  0.182129   1
2024-01-02 -0.705585  0.045678 -0.427188 -0.916598   2
2024-01-03 -0.252943 -1.099356  0.773502 -0.294143   3
2024-01-04  1.512004 -0.329588 -2.308834  0.007813   4
2024-01-05 -0.025248  0.814907  0.147681  0.816508   5
2024-01-06 -0.869568  1.597306  0.115712 -2.568746   6
```

Setting values by label:

```
>>> df.at[dates[0], 'A'] = 0
```

Setting values by position:

```
>>> df.iat[0, 1] = 1
```

Setting by assigning a Numpy array (basically a `Series`):

```
>>> df.loc[:, 'D'] = np.array([5] * len(df))
```

Final result after all the manipulations above:

```
>>> df
                   A         B         C    D  F
2024-01-01  0.000000  1.000000 -1.762178  5.0  1
2024-01-02 -0.705585  0.045678 -0.427188  5.0  2
2024-01-03 -0.252943 -1.099356  0.773502  5.0  3
2024-01-04  1.512004 -0.329588 -2.308834  5.0  4
2024-01-05 -0.025248  0.814907  0.147681  5.0  5
2024-01-06 -0.869568  1.597306  0.115712  5.0  6
```

## Missing data

For NumPy data types, `np.nan` represents missing data.

For example:

```
>>> df = pd.DataFrame(np.random.randn(6, 4), index=dates, columns=list("ABCD"))
>>> df
                   A         B         C         D
2024-01-01  0.959922  0.395782  0.135108 -0.245570
2024-01-02  1.675264  1.304140  0.406993  0.406932
2024-01-03  0.575230  2.325347 -0.666868 -1.699481
2024-01-04  1.001187 -0.548267 -1.194915 -0.257750
2024-01-05  0.172012  1.290922  0.054075 -0.398257
2024-01-06  0.597093  0.321969  0.493588 -1.298289
>>> df = df[df > 0]
>>> df
                   A         B         C         D
2024-01-01  0.959922  0.395782  0.135108       NaN
2024-01-02  1.675264  1.304140  0.406993  0.406932
2024-01-03  0.575230  2.325347       NaN       NaN
2024-01-04  1.001187       NaN       NaN       NaN
2024-01-05  0.172012  1.290922  0.054075       NaN
2024-01-06  0.597093  0.321969  0.493588       NaN
```

You can drop rows with missing data using `DataFrame.dropna()`:

```
>>> df.dropna()
                   A        B         C         D
2024-01-02  1.675264  1.30414  0.406993  0.406932
```

Or you can fill them with a specified value:

```
>>> df.fillna(0)
                   A         B         C         D
2024-01-01  0.959922  0.395782  0.135108  0.000000
2024-01-02  1.675264  1.304140  0.406993  0.406932
2024-01-03  0.575230  2.325347  0.000000  0.000000
2024-01-04  1.001187  0.000000  0.000000  0.000000
2024-01-05  0.172012  1.290922  0.054075  0.000000
2024-01-06  0.597093  0.321969  0.493588  0.000000
```

You can also use the `isna` function to check if an index is `NaN`:

```
>>> df.isna()
                A      B      C      D
2024-01-01  False  False  False   True
2024-01-02  False  False  False  False
2024-01-03  False  False   True   True
2024-01-04  False   True   True   True
2024-01-05  False  False  False   True
2024-01-06  False  False  False   True
```

## Operations

### Basic binary operations

There are two key points of interest when performing operations between Pandas data structures:

1. Broadcasting behavior between higher-dimensional (e.g. `DataFrame`) and lower-dimensional (e.g.
   `Series`) objects
2. Missing data in computations

`DataFrame` objects have `add()`, `sub()`, `mul()`, `div()`, and related functions `radd()`,
`rsub()`, ... for carrying out binary operations. For broadcasting behavior, the `Series` input is
important.

You can learn more about these basic operations 
[here](https://pandas.pydata.org/docs/user_guide/basics.html#basics-binop).

### Stats

Operations usually **exclude** missing data.

```
>>> df = pd.DataFrame(np.random.randn(6, 4), index=dates, columns=list("ABCD"))
>>> df
                   A         B         C         D
2024-01-01  0.504643  0.914445 -0.768722 -0.435614
2024-01-02  1.154947  1.724654 -0.052462  0.729591
2024-01-03 -0.296345  1.672518  1.121372  1.793861
2024-01-04 -0.715150 -1.499834  0.031721  0.874650
2024-01-05  1.559506 -0.452091  0.411096 -1.385334
2024-01-06  1.818255  1.034515 -0.039904 -0.128510
```

Calculate the mean for each column:

```
>>> df.mean()
A    0.670976
B    0.565701
C    0.117184
D    0.241441
dtype: float64
```

Calculate the mean for each row:

```
>>> df.mean(axis=1)
2024-01-01    0.053688
2024-01-02    0.889183
2024-01-03    1.072851
2024-01-04   -0.327153
2024-01-05    0.033295
2024-01-06    0.671089
Freq: D, dtype: float64
```

Operating with another `Series` or `DataFrame` with a different index or column will align the
result with the union of the index or column labels. Additionally, Pandas automatically broadcasts
along the specified dimension and will fill unaligned labels with `np.nan`.

```
>>> dates = pd.date_range("20240101", periods=6)
>>> s = pd.Series([1, 3, 5, np.nan, 6, 8], index=dates).shift(2)
>>> df.sub(s, axis="index")
                   A         B         C         D
2024-01-01       NaN       NaN       NaN       NaN
2024-01-02       NaN       NaN       NaN       NaN
2024-01-03 -1.296345  0.672518  0.121372  0.793861
2024-01-04 -3.715150 -4.499834 -2.968279 -2.125350
2024-01-05 -3.440494 -5.452091 -4.588904 -6.385334
2024-01-06       NaN       NaN       NaN       NaN
```

### User-defined functions

`DataFrame.agg()` and `DataFrame.transform()` apply a user-defined function that reduces or
broadcasts its result.

```
>>> df.agg(lambda x: np.mean(x) * 5.6)
A    3.757466
B    3.167927
C    0.656228
D    1.352067
dtype: float64
```

```
>>> df.transform(lambda x: x * 10)
                    A          B          C          D
2024-01-01   5.046433   9.144447  -7.687223  -4.356142
2024-01-02  11.549472  17.246540  -0.524619   7.295907
2024-01-03  -2.963455  16.725177  11.213718  17.938611
2024-01-04  -7.151502 -14.998337   0.317213   8.746496
2024-01-05  15.595064  -4.520906   4.110961 -13.853335
2024-01-06  18.182551  10.345154  -0.399037  -1.285100
```

### Value counts

See more in the 
[histogramming and discretization](https://pandas.pydata.org/docs/user_guide/basics.html#basics-discretization)
section.

```
>>> s = pd.Series(np.random.randint(0, 7, size=50))
>>> s.value_counts()
2    9
1    8
4    8
6    7
5    7
3    7
0    4
Name: count, dtype: int64
```

### String methods

`Series` have string processing methods. See more
[here](https://pandas.pydata.org/docs/user_guide/text.html#text-string-methods).

```
>>> s = pd.Series(["A", "B", "C", "Aaba", "Baca", np.nan, "CABA", "dog", "cat"])
>>> s.str.lower()
0       a
1       b
2       c
3    aaba
4    baca
5     NaN
6    caba
7     dog
8     cat
dtype: object
```

## Merge

See the [merging](https://pandas.pydata.org/docs/user_guide/merging.html#merging) section for more
details.

### Concat

`Series` and `DataFrame` objects can be combined using the `concat()` method:

```
>>> df = pd.DataFrame(np.random.randn(10, 4))
>>> df
          0         1         2         3
0 -0.537251 -0.081047  1.252963  1.041015
1  1.953332 -0.788358  0.781047 -0.844849
2  0.949162  0.505667 -2.034872  0.253387
3  0.902654  1.053329 -0.066962  0.548099
4 -0.894122  2.147863  0.436488  0.674850
5  0.048556 -1.511568  0.828026 -0.115534
6 -0.558155  0.739852 -0.597746  0.705634
7  0.817522 -0.734611  0.018743 -1.541225
8 -0.229119 -0.337087 -0.740469  0.147334
9  0.461353 -0.443583  1.835264  0.319544
>>> pieces = [df[7:], df[3:7], df[:3]]
>>> pd.concat(pieces)
          0         1         2         3
7  0.817522 -0.734611  0.018743 -1.541225
8 -0.229119 -0.337087 -0.740469  0.147334
9  0.461353 -0.443583  1.835264  0.319544
3  0.902654  1.053329 -0.066962  0.548099
4 -0.894122  2.147863  0.436488  0.674850
5  0.048556 -1.511568  0.828026 -0.115534
6 -0.558155  0.739852 -0.597746  0.705634
0 -0.537251 -0.081047  1.252963  1.041015
1  1.953332 -0.788358  0.781047 -0.844849
2  0.949162  0.505667 -2.034872  0.253387
```

### Join

The `merge()` method enables SQL-like joins along specific columns. See the 
[database-style joining](https://pandas.pydata.org/docs/user_guide/merging.html#merging-join) section.

```
>>> left = pd.DataFrame({"key": ["foo", "foo"], "lval": [1, 2]})
>>> right = pd.DataFrame({"key": ["foo", "foo"], "rval": [4, 5]})
>>> left
   key  lval
0  foo     1
1  foo     2
>>> right
   key  rval
0  foo     4
1  foo     5
>>> pd.merge(left, right, on="key")
   key  lval  rval
0  foo     1     4
1  foo     1     5
2  foo     2     4
3  foo     2     5
```

`merge()` on unique keys:

```
>>> left = pd.DataFrame({"key": ["foo", "bar"], "lval": [1, 2]})
>>> right = pd.DataFrame({"key": ["foo", "bar"], "rval": [4, 5]})
>>> pd.merge(left, right, on="key")
   key  lval  rval
0  foo     1     4
1  bar     2     5
```

## Grouping

When grouping is mentioned, the process involves one or more of the following steps:

- **Splitting** the data into groups based on criteria
- **Applying** a function to each group independently
- **Combining** the results into a data structure

See the [grouping](https://pandas.pydata.org/docs/user_guide/groupby.html#groupby) section.

```
>>> df = pd.DataFrame(
... 
...     {
... 
...         "A": ["foo", "bar", "foo", "bar", "foo", "bar", "foo", "foo"],
... 
...         "B": ["one", "one", "two", "three", "two", "two", "one", "three"],
... 
...         "C": np.random.randn(8),
... 
...         "D": np.random.randn(8),
... 
...     }
... 
... )
>>> df
     A      B         C         D
0  foo    one  0.574691 -0.389473
1  bar    one  0.031453  2.340871
2  foo    two -1.261001  0.561261
3  bar  three -0.478304  0.633623
4  foo    two  0.712457 -0.600925
5  bar    two -0.406352  0.209869
6  foo    one -0.147374  0.100432
7  foo  three -1.319855 -0.520183
```

Grouping by column label, selecting column labels, and then applying a function to resulting groups:

```
>>> df.groupby("A")[["C", "D"]].sum()
            C         D
A                      
bar -0.853203  3.184363
foo -1.441082 -0.848890
```

Grouping by multiple columns creates
[MultiIndex](https://pandas.pydata.org/docs/reference/api/pandas.MultiIndex.html#pandas.MultiIndex)
data structures.

```
>>> df.groupby(["A", "B"]).sum()
                  C         D
A   B                        
bar one    0.031453  2.340871
    three -0.478304  0.633623
    two   -0.406352  0.209869
foo one    0.427317 -0.289042
    three -1.319855 -0.520183
    two   -0.548544 -0.039665
```

## Categoricals

Pandas can include categorical data in a `DataFrame`. For full documentation, see
[categorical introduction](https://pandas.pydata.org/docs/user_guide/categorical.html#categorical)
and [API documentation](https://pandas.pydata.org/docs/reference/arrays.html#api-arrays-categorical).

```
>>> df = pd.DataFrame(
...     {"id": [1, 2, 3, 4, 5, 6], "raw_grade": ["a", "b", "b", "a", "a", "e"]}
... )
```

Converting the raw grades to a categorical type:

```
>>> df["grade"] = df["raw_grade"].astype("category")
>>> df["grade"]
0    a
1    b
2    b
3    a
4    a
5    e
Name: grade, dtype: category
Categories (3, object): ['a', 'b', 'e']
```

Renaming the categories to be something more meaningful:

```
>>> df["grade"] = df["grade"].cat.rename_categories(["very good", "good", "very bad"])
```

Reorder the categories and add "missing" categories:

```
>>> df["grade"] = df["grade"].cat.set_categories(["very bad", "bad", "medium", "good", "very good"])
>>> df["grade"]
0    very good
1         good
2         good
3    very good
4    very good
5     very bad
Name: grade, dtype: category
Categories (5, object): ['very bad', 'bad', 'medium', 'good', 'very good']
```

Now we are left with:

```
>>> df.sort_values(by="grade")
   id raw_grade      grade
5   6         e   very bad
1   2         b       good
2   3         b       good
0   1         a  very good
3   4         a  very good
4   5         a  very good
```

One other thing we can do is see how many people were placed in each category:

```
>>> df.groupby("grade", observed=False).size()
grade
very bad     1
bad          0
medium       0
good         2
very good    3
dtype: int64
```

## Plotting

https://pandas.pydata.org/docs/user_guide/10min.html#plotting

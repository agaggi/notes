# The UV package manager for Python

`uv` is a modern package manager for Python that aims to replace many independent tools such as:

- `pip`
- `pip-tools`
- `pipx`
- `poetry`
- `pyenv`
- `twine`
- `virtualenv`
- ... and more.

To get started with using `uv`, we can initialize a new project:

```
$ uv init <project-name>
```

This will create a new directory with the following structure:

```
.
├── hello.py
├── pyproject.toml
├── .python-version
└── README.md
```

> **Note**
> 
> `.python-version` is typically used by a tool called `pyenv`, which manages multiple versions of
> Python on yor system. This file will tell `pyenv` to switch to the Python version specified in
> `.python-version` if you enter this diretory.

> **Note**
> 
> `pyproject.toml` files are found in modern Python projects to specify various project and build
> settings according to PEP 517 and PEP 518 standards.
> 
> It can also be used to configure tools such as linters, etc.

Source: https://github.com/astral-sh/uv

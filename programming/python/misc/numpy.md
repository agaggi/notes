# NumPy library

Python lists are general-purpose containers that can be *heterogeneous*. They can hold multiple
types and are fast when performing individual operations on a handful of elements. Depending on the
characteristics of the data and the kinds of operations needed to be performed, other containers may
be more appropriate.

NumPy offers an alternative container for *homogeneous* data(same type) data that is faster, more
memory efficient, but still offers high-level syntax.

NumPy is an external Python package. It can be installed through `conda`, `pip`, or a package
manager on Linux/MacOS. You will need to import the library if you want to use it:

```python
import numpy as np
```

**Reference**: https://numpy.org/doc/stable/user/absolute_beginners.html

## The `numpy.ndarray` type

You can declare a NumPy array as follows:

```
>>> a = np.array(range(6))
>>> a
array([0, 1, 2, 3, 4, 5])
```

Arrays can be accessed and modified similarly to Python lists:

```
>>> a[0]
0
>>> a[:3]
array([0, 1, 2])
>>> a[0] = 10
>>> a
array([10,  1,  2,  3,  4,  5])
```

A major difference arrays have compared to lists is that slice indexing returns an object that still
references the data in the original array (i.e. shallow copy) rather than creating a new object:

```
>>> b = a[3:]
>>> b
array([3, 4, 5])
>>> b[2] *= 2
>>> a
array([10,  1,  2,  3,  4, 10])
```

NumPy also supports 2D and higher arrays:

```
>>> a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
>>> a
array([[ 1,  2,  3,  4],
       [ 5,  6,  7,  8],
       [ 9, 10, 11, 12]])
```

In NumPy, a dimension of an array is sometimes referred to an *axis*. The array above is a 2D array
so it has 2 axes. This is not to be confused with the dimensions of the *array data*, which above
happens to be 4-by-3.

Array elements can also be accessed by specifying an index alongside its axis:

```
>>> a[1, 3]
8
>>> a[0, 1]
2
```

## Array attributes

The number of dimensions of an array is contained in the `ndim` attribute:

```
>>> a.ndim
2
```

The `shape` of an array is a tuple that specifies the number of elements along each dimension:

```
>>> a.shape
(3, 4)
>>> len(a.shape) == a.ndim
True
```

The fixed, total number of elements in array is contained in the `size` attribute:

```
>>> a.size
12
>>> import math
>>> a.size == math.prod(a.shape)
True
```

Arrays are typically “homogeneous”; the data type is recorded in the `dtype` attribute:

```
>>> a.dtype
dtype('int64')  # "int" for integer, "64" for 64-bit
```

## Creating basic arrays

You can create pre-filled arrays using `np.zeros()` or `np.ones()`:

```
>>> np.zeros(2)
array([0., 0.])
>>> np.ones([4, 3], dtype=np.int32)
array([[1, 1, 1],
       [1, 1, 1],
       [1, 1, 1],
       [1, 1, 1]], dtype=int32)
```

You can also create "empty" arrays. The `empty` function creates an array full of random elements
that depend on the state of system memory. It's actually faster than the `zeros` or `ones` functions
but you have to remember to fill in every element afterwards:

```
>>> np.empty([4, 2])
array([[1.42182664e-315, 0.00000000e+000],
       [1.05161522e-153, 1.05194010e-153],
       [1.14920486e+141, 1.47900214e-076],
       [6.35226565e-067, 6.97361036e+228]])
```

You can create arrays using a range of elements:

```
>>> np.arange(2, 11, 2)
array([ 2,  4,  6,  8, 10])
```

You can also use `np.linspace()` to create an array with values that are spaced linearly in a
specified interval:

```
>>> np.linspace(0, 100, num=3)
array([  0.,  50., 100.])
>>> np.linspace(0, 100, num=4)
array([  0.        ,  33.33333333,  66.66666667, 100.        ])
```

## Adding, removing, and sorting elements

Sorting arrays is simple with `np.sort()`. You can specify the axis, kind, and order when you call
the function:

```
>>> a
array([[ 887036960,      32725,  887036960,      32725, 1952984631],
       [ 774769261, 1563581216, 1802065696, 1684369513,  539779433],
       [1735347234, 1953067617,  774008168, 1953785888,  792359792],
       [ 778986799, 1768647031, 1768187248, 1919888993, 1769418599],
       [1278175595, 1918986095, 1835562089, 2017790474, 1819307361]],
      dtype=int32)
>>> np.sort(a)
array([[     32725,      32725,  887036960,  887036960, 1952984631],
       [ 539779433,  774769261, 1563581216, 1684369513, 1802065696],
       [ 774008168,  792359792, 1735347234, 1953067617, 1953785888],
       [ 778986799, 1768187248, 1768647031, 1769418599, 1919888993],
       [1278175595, 1819307361, 1835562089, 1918986095, 2017790474]],
      dtype=int32)
>>> np.sort(a, axis=0)
array([[ 774769261,      32725,  774008168,      32725,  539779433],
       [ 778986799, 1563581216,  887036960, 1684369513,  792359792],
       [ 887036960, 1768647031, 1768187248, 1919888993, 1769418599],
       [1278175595, 1918986095, 1802065696, 1953785888, 1819307361],
       [1735347234, 1953067617, 1835562089, 2017790474, 1952984631]],
      dtype=int32)
```

See what other ways you can sort 
[here](https://numpy.org/doc/stable/reference/generated/numpy.sort.html#numpy.sort).

Arrays can be concatenated together:

```
>>> a = np.array([1, 2, 3, 4])
>>> b = np.array([5, 6, 7, 8])
>>> np.concatenate((a, b))
array([1, 2, 3, 4, 5, 6, 7, 8])

>>> x = np.array([[1, 2], [3, 4]])
>>> y = np.array([[5, 6]])
>>> np.concatenate((x, y), axis=0)
array([[1, 2],
       [3, 4],
       [5, 6]])
```

## Can you reshape an array?

The answer is *yes* using `np.reshape`:

```
>>> a = np.arange(10)
>>> a
array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
>>> a = a.reshape(2, 5)
>>> a
array([[0, 1, 2, 3, 4],
       [5, 6, 7, 8, 9]])
```

If you are familiar with matricies, you can also transpose a 2D array (switching the axis) by using
`.transpose()` or `.T`:

```
>>> a = np.arange(1, 10)
>>> a
array([1, 2, 3, 4, 5, 6, 7, 8, 9])
>>> a = a.reshape((3, 3))
>>> a
array([[1, 2, 3],
       [4, 5, 6],
       [7, 8, 9]])
>>> a.T
array([[1, 4, 7],
       [2, 5, 8],
       [3, 6, 9]])
```

## Converting a 1D array into a 2D array

`np.newaxis` will increase the dimensions of an array by one (e.g., 1D -> 2D):

```
>>> a = np.arange(1, 7)
>>> a.shape
(6,)
>>> a2 = a[np.newaxis, :]
>>> a2.shape
(1, 6)
>>> a2
array([[1, 2, 3, 4, 5, 6]])
>>> a3 = a[:, np.newaxis]
>>> a3.shape
(6, 1)
>>> a3
array([[1],
       [2],
       [3],
       [4],
       [5],
       [6]])
```

## Additional indexing and slicing

In addition to typical list slicing and indexing, you can select values that fulfill a certain
condition:

```
>>> a = np.array([[1 , 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
>>> a[a < 5]
array([1, 2, 3, 4])
>>> a[(a >= 5) & (a < 10)]
array([5, 6, 7, 8, 9])
```

You can use these conditions outside of filtering elements to have an array of boolean values:

```
>>> a
array([[ 1,  2,  3,  4],
       [ 5,  6,  7,  8],
       [ 9, 10, 11, 12]])
>>> a > 5
array([[False, False, False, False],
       [False,  True,  True,  True],
       [ True,  True,  True,  True]])
```

## Creating arrays from existing data

One way you can create new arrays is stacking two arrays together either vertically or horizontally:

```
>>> a = np.array([[1 , 2], [ 3, 4]])
>>> b = np.array([[5 , 6], [7, 8]])
>>> np.vstack((a, b))
array([[1, 2],
       [3, 4],
       [5, 6],
       [7, 8]])
>>> np.hstack((a, b))
array([[1, 2, 5, 6],
       [3, 4, 7, 8]])
```

## Basic array options

Addition, subtraction, multiplication, division:

```
>>> np.array([1, 2]) + np.ones(2)
array([2., 3.])
>>> np.array([1, 2]) - np.ones(2)
array([0., 1.])
>>> np.array([1, 2]) ** 2
array([1, 4])
>>> np.array([1, 2]) // 2
array([0, 1])
```

There are a number of helper functions you can use with arrays as well:

```
>>> np.array([1, 2]).sum()
3
>>> np.array([1, 2]).min()
1
>>> np.array([1, 2]).max()
2
```

See more helper functions 
[here](https://numpy.org/doc/stable/user/quickstart.html#quickstart-basic-operations).

## Matricies

Matricies are essentially 2D Python lists. For example:

```
>>> np.random.randint(256, size=(8, 3), dtype=np.uint8)
array([[ 46, 249,  46],
       [ 26, 124,  48],
       [160,  56, 161],
       [112,   2, 168],
       [ 19, 197, 171],
       [136, 174, 221],
       [190, 230, 163],
       [ 55, 222,  63]], dtype=uint8)
```

You can aggregate all values in a matrix and you can aggregate values in columns or rows using the
axis parameter. For example:

```
>>> a = np.array([[1, 2], [3, 4], [5, 6]])
>>> a.max(axis=0)
array([5, 6])
>>> a.max(axis=1)
array([2, 4, 6])
```

## Getting unique items and counts

You can find unique items in an array using `np.unique`:

```
>>> a = np.array([11, 11, 12, 13, 14, 15, 16, 17, 12, 13, 11, 14, 18, 19, 20])
>>> np.unique(a)
array([11, 12, 13, 14, 15, 16, 17, 18, 19, 20])
>>> np.unique(a, return_index=True)
(array([11, 12, 13, 14, 15, 16, 17, 18, 19, 20]), array([ 0,  2,  3,  4,  5,  6,  7, 12, 13, 14]))
```

This also works for 2D arrays:

```
>>> a_2d = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [1, 2, 3, 4]])
>>> np.unique(a_2d)
array([ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12])
>>> np.unique(a_2d, axis=0)
array([[ 1,  2,  3,  4],
       [ 5,  6,  7,  8],
       [ 9, 10, 11, 12]])
>>> np.unique(a_2d, axis=1)
array([[ 1,  2,  3,  4],
       [ 5,  6,  7,  8],
       [ 9, 10, 11, 12],
       [ 1,  2,  3,  4]])
```

You can also show how many times something occurs:

```
>>> np.unique(a_2d, return_counts=True, axis=0)
(array([[ 1,  2,  3,  4],
       [ 5,  6,  7,  8],
       [ 9, 10, 11, 12]]), array([2, 1, 1]))
```

## Reversing arrays

The `np.flip` function allows you to "flip", or reverse, the contents of an array along its axis. By
default, NumPy will reverse all axes:

```
>>> a = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
>>> np.flip(a)
array([[9, 8, 7],
       [6, 5, 4],
       [3, 2, 1]])
>>> np.flip(a, axis=0)
array([[7, 8, 9],
       [4, 5, 6],
       [1, 2, 3]])
>>> np.flip(a, axis=1)
array([[3, 2, 1],
       [6, 5, 4],
       [9, 8, 7]])
```

You can reverse certain rows or columns as well:

```
>>> a[1] = np.flip(a[1])
>>> a
array([[1, 2, 3],
       [6, 5, 4],
       [7, 8, 9]])
>>> a[:, 1] = np.flip(a[:, 1])
>>> a
array([[1, 8, 3],
       [6, 5, 4],
       [7, 2, 9]])
```

## Reshaping and flattening multi-dimensional arrays

There are two popular ways to flatten arrays: `.flatten()` and `.ravel()`. The primary difference
between the two is that `.ravel()` creates a new array that references the parent array. This means
any changes done to the new array affects the original array (i.e., shallow copy.) Since `.ravel()`
does not create a copy, it's more memory efficient.

```
>>> a = np.array([[1 , 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
>>> a.flatten()
array([ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12])
>>> a2 = a.ravel()
>>> a[0][0] = 20
>>> a
array([[20,  2,  3,  4],
       [ 5,  6,  7,  8],
       [ 9, 10, 11, 12]])
>>> a2
array([20,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12])
```

## Loading and saving NumPy objects

At some point you will want to save arrays to disk and load them back without having to re-run code
each time. There are several ways to save and load NumPy objects:

1. `ndaray` objects can be saved to and loaded from text files using `loadtxt` and `savetxt`
   functions
2. `load` and `save` functions that handle NumPy binaries with a **.npy** file extension
3. A `savez` function that handles NumPy files with a **.npz** file extension

**.npy** and **.npz** files store data, shape, dtype, and other information required to reconstruct
an array even when the file is on another machine with a different architecture.

```
>>> a = np.array([1, 2, 3, 4, 5, 6])
>>> np.save('test', a)
>>> b = np.load('test.npy')
>>> b
array([1, 2, 3, 4, 5, 6])
>>> np.savetxt('test.csv', a)
>>> b = np.loadtxt('test.csv')
>>> b
array([1., 2., 3., 4., 5., 6.])
```

## Additional resources

- [NumPy data types](https://numpy.org/doc/stable/user/basics.types.html#data-types)
- [Working with strings and bytes](https://numpy.org/doc/stable/user/basics.strings.html#working-with-arrays-of-strings-and-bytes)
- [Structured data type arrays](https://numpy.org/doc/stable/user/basics.rec.html#structured-arrays)

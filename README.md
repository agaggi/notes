# Notes

This is a repository for all of my personal notes and some resources I use to learn.

The contents of the `master` branch are resources and completed notes... at least for now. There are
other branches, labeled `wip/*` that contain notes that are a **w**ork-**i**n-**p**rogress.
